package cz.cvut.nahlasto.dao;

import cz.cvut.nahlasto.model.Defect;
import cz.cvut.nahlasto.model.enums.DefectStatus;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class DefectDaoTest {
    @Autowired
    private DefectDao defectDao;

    @Test
    public void findAllDefectsReturnsNotEmptyList() {
        List<Defect> defectList = defectDao.findAll();
        assertTrue(!defectList.isEmpty());
    }

    @Test
    public void findDefectByIdReturnsDefect() {
        Defect defect = defectDao.find(1);
        assertNotNull(defect);
    }

    /*@Test
    public void createDefectShouldNotFail() {
        Defect myDefect = new Defect("myDefectThatCouldNotHaveBeenCreatedBeforeForSure", "desc", LocalDate.now(), null, DefectStatus.WAITING, null, null, null, null, null, null);
        defectDao.create(myDefect);
    }*/

    /*@Test
    public void createdDefectIsFoundByTitle() {
        Defect myDefect = new Defect("myDefectThatCouldNotHaveBeenCreatedBeforeForSure", "desc", LocalDate.now(), null, DefectStatus.WAITING, null, null, null, null, null, null);
        defectDao.create(myDefect);
        Defect foundDefect = defectDao.findDefectByTitle("myDefectThatCouldNotHaveBeenCreatedBeforeForSure");
        assertNotNull(foundDefect);
        assertEquals(myDefect.getTitle(), foundDefect.getTitle());
    }*/

//    @Test
//    public void waitingDefectAreFoundByStatus(){
//        Defect myDefect = new Defect("myDefectThatCouldNotHaveBeenCreatedBeforeForSure", "desc", LocalDate.now(), null, DefectStatus.WAITING, null, null, null, null);
//        defectDao.create(myDefect);
//        List<Defect> defectList = defectDao.findAllDefectsByStatus(DefectStatus.WAITING);
//        assertNotNull(defectList);
//    }
}