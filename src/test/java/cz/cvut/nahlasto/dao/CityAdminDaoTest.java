/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.nahlasto.dao;

import cz.cvut.nahlasto.model.CityAdmin;
import java.util.List;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)

public class CityAdminDaoTest {
        
    @Autowired
    private CityAdminDao cityAdminDao;

    @Test
    public void findAllCityAdminsReturnsNotEmptyList() {
        List<CityAdmin> cityAdminList = cityAdminDao.findAll();
        assertTrue(!cityAdminList.isEmpty());

    }

    @Test
    public void findCityAdminByIdReturnsDefect() {
        CityAdmin cityAdmin = cityAdminDao.find(1);
        assertNotNull(cityAdmin);
    }
}
