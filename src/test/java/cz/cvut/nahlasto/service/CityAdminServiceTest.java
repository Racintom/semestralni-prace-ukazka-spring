/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.nahlasto.service;

import cz.cvut.nahlasto.dao.AbstractDao;
import cz.cvut.nahlasto.dao.CityAdminDao;
import cz.cvut.nahlasto.model.Admin;
import cz.cvut.nahlasto.model.City;
import cz.cvut.nahlasto.model.CityAdmin;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;

import cz.cvut.nahlasto.model.Defect;
import org.hibernate.PropertyValueException;
import org.junit.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.internal.configuration.injection.MockInjection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class CityAdminServiceTest {

    @Mock
    private CityAdminDao cityAdminDao;

    @InjectMocks
    private CityAdminService cityAdminService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    private CityAdmin findCityAdmin(List<CityAdmin> cityAdmins, Integer id) {
        for (CityAdmin c : cityAdmins) {
            if (c.getId() == id) {
                return c;
            }
        }
        return null;
    }

    @Test(expected = PropertyValueException.class)
    public void createCityAdminThrowExceptionTest() {
        CityAdmin cityAdmin = new CityAdmin();

        Mockito.doThrow(PropertyValueException.class).when(cityAdminDao).create(cityAdmin);

        cityAdminService.create(cityAdmin);
    }

    @Test
    public void creatCityAdminTest() {
        int id = 1;
        CityAdmin cityAdmin = new CityAdmin();
        cityAdmin.setId(id);
        List<CityAdmin> cityAdmins = new ArrayList<>();

        Mockito.doAnswer((i) -> {
            cityAdmins.add(i.getArgument(0));
            return null;
        }).when(cityAdminDao).create(cityAdmin);

        cityAdminService.create(cityAdmin);

        assertFalse(cityAdmins.isEmpty());
    }

    @Test
    public void updateCityAdminShouldNotFail() {
        List<CityAdmin> cityAdmins = new ArrayList<>();
        int id = 1;
        CityAdmin cityAdmin = new CityAdmin(new City(), null, null);
        cityAdmin.setFirstName("FNAME");
        cityAdmin.setEmail("email");
        cityAdmin.setLastName("LNAME");
        cityAdmin.setPassword("PASSWD");
        cityAdmin.setUserName("UNAME");
        cityAdmin.setId(id);

        cityAdmins.add(cityAdmin);

        cityAdmin.setEmail("changedEmail");
        cityAdmin.setFirstName("new FNAME");

        Mockito.doAnswer(invocationOnMock -> {

            CityAdmin c = findCityAdmin(cityAdmins, ((CityAdmin) invocationOnMock.getArgument(0)).getId());
            c.setEmail(((CityAdmin) invocationOnMock.getArgument(0)).getEmail());
            c.setFirstName(((CityAdmin) invocationOnMock.getArgument(0)).getFirstName());
            c.setLastName(((CityAdmin) invocationOnMock.getArgument(0)).getLastName());
            c.setPassword(((CityAdmin) invocationOnMock.getArgument(0)).getPassword());
            c.setUserName(((CityAdmin) invocationOnMock.getArgument(0)).getUserName());
            return null;
        }).when(cityAdminDao).update(cityAdmin);
        Mockito.when(cityAdminDao.find(id)).thenReturn(cityAdmins.get(0));

        cityAdminService.update(cityAdmin);

        assertEquals(cityAdmin, findCityAdmin(cityAdmins, cityAdmin.getId()));

    }

    @Test
    public void removeCityAdmin() {
        List<CityAdmin> cityAdmins = new ArrayList<>();
        int id = 1;
        CityAdmin cityAdmin = new CityAdmin(new City(), null, null);
        cityAdmin.setFirstName("FNAME");
        cityAdmin.setEmail("email");
        cityAdmin.setLastName("LNAME");
        cityAdmin.setPassword("PASSWD");
        cityAdmin.setUserName("UNAME");
        cityAdmin.setId(id);

        cityAdmins.add(cityAdmin);

        Mockito.doAnswer(invocationOnMock -> {
            cityAdmins.remove(invocationOnMock.getArgument(0));
            return null;
        }).when(cityAdminDao).remove(cityAdmin);

        cityAdminService.remove(cityAdmin);

        Assert.assertFalse(cityAdmins.contains(cityAdmin));
    }

    @Test
    public void findCityAdminTest() {
        int id = 1;
        CityAdmin cityAdmin = new CityAdmin(new City(), null, null);
        cityAdmin.setFirstName("FNAME");
        cityAdmin.setEmail("email");
        cityAdmin.setLastName("LNAME");
        cityAdmin.setPassword("PASSWD");
        cityAdmin.setUserName("UNAME");
        cityAdmin.setId(id);

        Mockito.when(cityAdminDao.find(id)).thenReturn(cityAdmin);

        CityAdmin res = cityAdminService.findById(id);

        assertNotNull(res);
        assertEquals(cityAdmin, res);
    }

    @Test
    public void findAllCityAdminsTest() {
        List<CityAdmin> cityAdmins = new ArrayList<>();
        cityAdmins.add(new CityAdmin());
        cityAdmins.add(new CityAdmin());
        cityAdmins.add(new CityAdmin());

        Mockito.when(cityAdminService.findAll()).thenReturn(cityAdmins);

        assertFalse(cityAdmins.isEmpty());
    }

}
