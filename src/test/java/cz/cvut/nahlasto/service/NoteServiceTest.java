/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.nahlasto.service;

import cz.cvut.nahlasto.dao.NoteDao;
import cz.cvut.nahlasto.model.Note;
import org.hibernate.PropertyValueException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class NoteServiceTest {

    @Mock
    private NoteDao noteDao;

    @InjectMocks
    private NoteService noteService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    private Note findNote(List<Note> notes, Integer id) {
        for (Note note : notes) {
            if (note.getId() == id) {
                return note;
            }
        }
        return null;
    }

    @Test(expected = PropertyValueException.class)
    public void createNoteThrowExceptionTest() {
        Note note = new Note();

        Mockito.doThrow(PropertyValueException.class).when(noteDao).create(note);

        noteService.create(note);
    }

    @Test
    public void creatNoteTest() {
        int id = 1;
        Note note = new Note();
        note.setId(id);
        List<Note> cityAdmins = new ArrayList<>();

        Mockito.doAnswer((i) -> {
            cityAdmins.add(i.getArgument(0));
            return null;
        }).when(noteDao).create(note);

        noteService.create(note);

        assertFalse(cityAdmins.isEmpty());
    }

    @Test
    public void updateNoteShouldNotFail() {
        List<Note> cityAdmins = new ArrayList<>();
        int id = 1;
        Note note = new Note(true, "Note text", null, null);
        note.setId(id);

        cityAdmins.add(note);

        note.setText("New edited text");

        Mockito.doAnswer(invocationOnMock -> {
            Note c = findNote(cityAdmins, ((Note) invocationOnMock.getArgument(0)).getId());
            c.setText(((Note) invocationOnMock.getArgument(0)).getText());
            return null;
        }).when(noteDao).update(note);
        Mockito.when(noteDao.find(id)).thenReturn(cityAdmins.get(0));

        noteService.update(note);

        assertEquals(note, findNote(cityAdmins, note.getId()));

    }

    @Test
    public void removeNote() {
        List<Note> notes = new ArrayList<>();
        int id = 1;
        Note note = new Note(true, "Note text", null, null);
        note.setId(id);

        notes.add(note);

        Mockito.doAnswer(invocationOnMock -> {
            notes.remove(invocationOnMock.getArgument(0));
            return null;
        }).when(noteDao).remove(note);

        noteService.remove(note);

        assertFalse(notes.contains(note));
    }

    @Test
    public void findNoteTest() {
        int id = 1;
        Note note = new Note(true, "Note text", null, null);
        note.setId(id);

        Mockito.when(noteDao.find(id)).thenReturn(note);

        Note res = noteService.findById(id);

        assertNotNull(res);
        assertEquals(note, res);
    }

    @Test
    public void findAllNotesTest() {
        List<Note> notes = new ArrayList<>();
        notes.add(new Note());
        notes.add(new Note());
        notes.add(new Note());

        Mockito.when(noteService.findAll()).thenReturn(notes);

        assertFalse(notes.isEmpty());
    }
}
