/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.nahlasto.service;

import cz.cvut.nahlasto.dao.CityDao;
import cz.cvut.nahlasto.model.City;
import org.hibernate.PropertyValueException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class CityServiceTest {

    @Mock
    private CityDao cityDao;

    @InjectMocks
    private CityService cityService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    private City findCity(List<City> cities, Integer id) {
        for (City c : cities) {
            if (c.getId() == id) {
                return c;
            }
        }
        return null;
    }

    @Test(expected = PropertyValueException.class)
    public void createCityServiceThrowExceptionTest() {
        City city = new City();

        Mockito.doThrow(PropertyValueException.class).when(cityDao).create(city);

        cityService.create(city);
    }

    @Test
    public void creatCityServiceTest() {
        int id = 1;
        City city = new City();
        city.setId(id);
        List<City> cityAdmins = new ArrayList<>();

        Mockito.doAnswer((i) -> {
            cityAdmins.add(i.getArgument(0));
            return null;
        }).when(cityDao).create(city);

        cityService.create(city);

        assertFalse(cityAdmins.isEmpty());
    }

    @Test
    public void updateCityServiceShouldNotFail() {
        List<City> cities = new ArrayList<>();
        int id = 1;
        City city = new City("CNAME", "19700", "Reg", "CWEB", "CDESC", null, null);

        city.setId(id);

        cities.add(city);

        city.setName("NEW NAME");
        city.setRegion("NEW REGION");

        Mockito.doAnswer(invocationOnMock -> {

            City c = findCity(cities, ((City) invocationOnMock.getArgument(0)).getId());
            c.setName(((City) invocationOnMock.getArgument(0)).getName());
            c.setRegion(((City) invocationOnMock.getArgument(0)).getRegion());
            c.setWebPage(((City) invocationOnMock.getArgument(0)).getWebPage());
            c.setPostCode(((City) invocationOnMock.getArgument(0)).getPostCode());
            c.setDescription(((City) invocationOnMock.getArgument(0)).getDescription());
            return null;
        }).when(cityDao).update(city);
        Mockito.when(cityDao.find(id)).thenReturn(cities.get(0));

        cityService.update(city);

        assertEquals(city, findCity(cities, city.getId()));

    }

    @Test
    public void removeCityService() {
        List<City> cityAdmins = new ArrayList<>();
        int id = 1;
        City city = new City("CNAME", "19700", "Reg", "CWEB", "CDESC", null, null);

        city.setId(id);

        cityAdmins.add(city);

        Mockito.doAnswer(invocationOnMock -> {
            cityAdmins.remove(invocationOnMock.getArgument(0));
            return null;
        }).when(cityDao).remove(city);

        cityService.remove(city);

        assertFalse(cityAdmins.contains(city));
    }

    @Test
    public void findCityServiceTest() {
        int id = 1;
        City city = new City("CNAME", "19700", "Reg", "CWEB", "CDESC", null, null);


        Mockito.when(cityDao.find(id)).thenReturn(city);

        City res = cityService.findById(id);

        assertNotNull(res);
        assertEquals(city, res);
    }

    @Test
    public void findAllCityServicesTest() {
        List<City> cityAdmins = new ArrayList<>();
        cityAdmins.add(new City());
        cityAdmins.add(new City());
        cityAdmins.add(new City());

        Mockito.when(cityService.findAll()).thenReturn(cityAdmins);

        assertFalse(cityAdmins.isEmpty());
    }
}
