package cz.cvut.nahlasto.service;

import cz.cvut.nahlasto.dao.DefectDao;
import cz.cvut.nahlasto.model.Defect;
import cz.cvut.nahlasto.model.enums.DefectStatus;
import org.hibernate.PropertyValueException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

import org.junit.Before;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class DefectServiceTest {

    @Mock
    private DefectDao defectDao;

    @InjectMocks
    private DefectService defectService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    private Defect findDefect(List<Defect> cityAdmins, Integer id) {
        for (Defect c : cityAdmins) {
            if (c.getId() == id) {
                return c;
            }
        }
        return null;
    }

    @Test(expected = PropertyValueException.class)
    public void createDefectThrowExceptionTest() {
        Defect defect = new Defect();

        Mockito.doThrow(PropertyValueException.class).when(defectDao).create(defect);

        defectService.create(defect);
    }

    @Test
    public void creatDefectTest() {
        int id = 1;
        Defect defect = new Defect();
        defect.setId(id);
        List<Defect> cityAdmins = new ArrayList<>();

        Mockito.doAnswer((i) -> {
            cityAdmins.add(i.getArgument(0));
            return null;
        }).when(defectDao).create(defect);

        defectService.create(defect);

        assertFalse(cityAdmins.isEmpty());
    }

    @Test
    public void updateDefectShouldNotFail() {
        List<Defect> cityAdmins = new ArrayList<>();
        int id = 1;
        Defect defect = new Defect("TITLE", "DESC", LocalDate.MAX, "reporter@email.cz",
                DefectStatus.DUPLICATE, null, null, null, new ArrayList<>(), new ArrayList<>(), new ArrayList<>());
        defect.setId(id);

        cityAdmins.add(defect);

        defect.setTitle("NEW TITLE");
        defect.setDescription("NEW DESC");

        Mockito.doAnswer(invocationOnMock -> {
            Defect c = findDefect(cityAdmins, ((Defect) invocationOnMock.getArgument(0)).getId());
            c.setDescription(((Defect) invocationOnMock.getArgument(0)).getDescription());
            c.setTitle(((Defect) invocationOnMock.getArgument(0)).getTitle());
            c.setReporterEmail(((Defect) invocationOnMock.getArgument(0)).getReporterEmail());
            c.setStatus(((Defect) invocationOnMock.getArgument(0)).getStatus());
            return null;
        }).when(defectDao).update(defect);
        Mockito.when(defectDao.find(id)).thenReturn(cityAdmins.get(0));

        defectService.update(defect);

        assertEquals(defect, findDefect(cityAdmins, defect.getId()));

    }

    @Test
    public void removeDefect() {
        List<Defect> cityAdmins = new ArrayList<>();
        int id = 1;
        Defect defect = new Defect("TITLE", "DESC", LocalDate.MAX, "reporter@email.cz",
                DefectStatus.DUPLICATE, null, null, null, new ArrayList<>(), new ArrayList<>(), new ArrayList<>());
        defect.setId(id);

        cityAdmins.add(defect);

        Mockito.doAnswer(invocationOnMock -> {
            cityAdmins.remove(invocationOnMock.getArgument(0));
            return null;
        }).when(defectDao).remove(defect);

        defectService.remove(defect);

        assertFalse(cityAdmins.contains(defect));
    }

    @Test
    public void findDefectTest() {
        int id = 1;
        Defect defect = new Defect("TITLE", "DESC", LocalDate.MAX, "reporter@email.cz",
                DefectStatus.DUPLICATE, null, null, null, new ArrayList<>(), new ArrayList<>(), new ArrayList<>());
        defect.setId(id);

        Mockito.when(defectDao.find(id)).thenReturn(defect);

        Defect res = defectService.findById(id);

        assertNotNull(res);
        assertEquals(defect, res);
    }

    @Test
    public void findAllDefectsTest() {
        List<Defect> cityAdmins = new ArrayList<>();
        cityAdmins.add(new Defect());
        cityAdmins.add(new Defect());
        cityAdmins.add(new Defect());

        Mockito.when(defectService.findAll()).thenReturn(cityAdmins);

        assertFalse(cityAdmins.isEmpty());
    }
}
