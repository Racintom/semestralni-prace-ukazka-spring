package cz.cvut.nahlasto.service;

import cz.cvut.nahlasto.dao.RepairServiceDao;
import cz.cvut.nahlasto.model.RepairService;
import org.hibernate.PropertyValueException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class RepairServiceServiceTest {
    
    @Mock
    private RepairServiceDao repairServiceDao;

    @InjectMocks
    private RepairServiceService repairServiceService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }


    private RepairService findRepairService(List<RepairService> repairServices, Integer id) {
        for (RepairService repairService : repairServices) {
            if (repairService.getId() == id) {
                return repairService;
            }
        }
        return null;
    }

    @Test(expected = PropertyValueException.class)
    public void createRepairServiceThrowExceptionTest() {
        RepairService repairService = new RepairService();

        Mockito.doThrow(PropertyValueException.class).when(repairServiceDao).create(repairService);

        repairServiceService.create(repairService);
    }

    @Test
    public void creatRepairServiceTest() {
        int id = 1;
        RepairService repairService = new RepairService();
        repairService.setId(id);
        List<RepairService> repairServices = new ArrayList<>();

        Mockito.doAnswer((i) -> {
            repairServices.add(i.getArgument(0));
            return null;
        }).when(repairServiceDao).create(repairService);

        repairServiceService.create(repairService);

        assertFalse(repairServices.isEmpty());
    }

    @Test
    public void updateRepairServiceShouldNotFail() {
        List<RepairService> cityAdmins = new ArrayList<>();
        int id = 1;
        RepairService repairService = new RepairService("Repair service", "email@email.cz", "123456789", null, null);
        repairService.setId(id);

        cityAdmins.add(repairService);

        repairService.setName("New name");
        repairService.setEmail("New email");
        repairService.setPhone("987654321");

        Mockito.doAnswer(invocationOnMock -> {
            RepairService c = findRepairService(cityAdmins, ((RepairService) invocationOnMock.getArgument(0)).getId());
            c.setName(((RepairService) invocationOnMock.getArgument(0)).getName());
            c.setEmail(((RepairService) invocationOnMock.getArgument(0)).getEmail());
            c.setPhone(((RepairService) invocationOnMock.getArgument(0)).getPhone());
            return null;
        }).when(repairServiceDao).update(repairService);
        Mockito.when(repairServiceDao.find(id)).thenReturn(cityAdmins.get(0));

        repairServiceService.update(repairService);

        assertEquals(repairService, findRepairService(cityAdmins, repairService.getId()));

    }

    @Test
    public void removeRepairService() {
        List<RepairService> notes = new ArrayList<>();
        int id = 1;
        RepairService repairService = new RepairService("Repair service", "email@email.cz", "123456789", null, null);
        repairService.setId(id);

        notes.add(repairService);

        Mockito.doAnswer(invocationOnMock -> {
            notes.remove(invocationOnMock.getArgument(0));
            return null;
        }).when(repairServiceDao).remove(repairService);

        repairServiceService.remove(repairService);

        assertFalse(notes.contains(repairService));
    }

    @Test
    public void findRepairServiceTest() {
        int id = 1;
        RepairService repairService = new RepairService("Repair service", "email@email.cz", "123456789", null, null);
        repairService.setId(id);

        Mockito.when(repairServiceDao.find(id)).thenReturn(repairService);

        RepairService res = repairServiceService.findById(id);

        assertNotNull(res);
        assertEquals(repairService, res);
    }

    @Test
    public void findAllRepairServicesTest() {
        List<RepairService> repairServices = new ArrayList<>();
        repairServices.add(new RepairService());
        repairServices.add(new RepairService());
        repairServices.add(new RepairService());

        Mockito.when(repairServiceService.findAll()).thenReturn(repairServices);

        assertFalse(repairServices.isEmpty());
    }
}