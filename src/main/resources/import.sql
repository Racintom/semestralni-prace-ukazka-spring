-- Města
INSERT INTO ear2018zs_34.public.city (description, city_name, post_code, region, web_page, token) VALUES ('Popisek města Prahy 6', 'Praha', '16000', 'Praha', 'https://www.praha6.cz/', 'Praha16000');
INSERT INTO ear2018zs_34.public.city (description, city_name, post_code, region, web_page, token) VALUES ('Popisek města Brno', 'Brno', '11111', 'Jihomoravský kraj', 'https://www.brno.cz/uvodni-strana/', 'Brno11111');

-- CityAdmin
-- františek vomáčka - heslo frvomacka1
-- pepa zdepa - heslo pepazdepa
INSERT INTO ear2018zs_34.public.city_admin (email, first_name, last_name, password, user_name, city_id) VALUES ('cityadmin1@mail.com', 'František', 'Vomáčka', '$2a$10$iBk1ILLch805jizNJRnHOui06ZEHaHSAY9TunZmxNCdVwb3nEBTXy', 'frantav', '1');
INSERT INTO ear2018zs_34.public.city_admin (email, first_name, last_name, password, user_name, city_id) VALUES ('cityadmin2@mail.com', 'Pepa', 'Zdepa', '$2a$10$.nDjZRg8H7O5iXA0H2MWM.8BVi.syCbpU38IY4duGS5DFoJF2AJYq', 'pepicek', '2');

-- Defekty
INSERT INTO ear2018zs_34.public.defect (city, house_number, postcode, road, suburb, date_created, description, located_using, lat, lon, reporter_email, status, title, city_id) VALUES ('Praha', NULL, '16000', 'Technická', NULL, '2017-05-27', 'Popisek defektu 1', 'GPS', 50.102457, 14.392143, 'reporter1@email.com', 'WAITING', 'Titulek defektu 1', 1);
INSERT INTO ear2018zs_34.public.defect (date_created, description, located_using, manual_location, reporter_email, status, title, city_id) VALUES ('2018-01-09', 'Popisek defektu 2', 'TEXT', 'V parku u sochy', 'reporter2@seznam.cz', 'APPROVED', 'Titulek defektu 2', 1);
INSERT INTO ear2018zs_34.public.defect (date_created, description, located_using, manual_location, reporter_email, status, title, city_id) VALUES ('2019-03-11', 'Popisek defektu 3', 'TEXT', 'V parku u sochy', 'reporter3@gmail.com', 'DECLINED', 'Titulek defektu 3', 1);
INSERT INTO ear2018zs_34.public.defect (date_created, description, located_using, manual_location, reporter_email, status, title, city_id) VALUES ('2019-03-21', 'Popisek defektu 4', 'TEXT', 'V parku u sochy', 'reporter4@centrum.cz', 'COMPLETED', 'Titulek defektu 4', 1);
INSERT INTO ear2018zs_34.public.defect (date_created, description, located_using, manual_location, reporter_email, status, title, city_id) VALUES ('2019-04-01', 'Popisek defektu 5', 'TEXT', 'V parku u sochy', 'reporter5@centrum.cz', 'IN_PROGRESS', 'Titulek defektu 5', 1);
INSERT INTO ear2018zs_34.public.defect (date_created, description, located_using, manual_location, reporter_email, status, title, city_id) VALUES (CURRENT_DATE, 'Popisek defektu 5', 'TEXT', 'V parku u sochy', 'reporter5@centrum.cz', 'DUPLICATE', 'Titulek defektu 5', 2);
INSERT INTO ear2018zs_34.public.defect (date_created, description, located_using, manual_location, reporter_email, status, title, city_id) VALUES (CURRENT_DATE, 'Popisek defektu 6', 'TEXT', 'V parku u sochy', 'reporter5@centrum.cz', 'DUPLICATE', 'Titulek defektu 6', 2);
INSERT INTO ear2018zs_34.public.defect (date_created, description, located_using, manual_location, reporter_email, status, title, city_id) VALUES (CURRENT_DATE, 'Popisek defektu 7', 'TEXT', 'V parku u sochy', 'reporter5@centrum.cz', 'DUPLICATE', 'Titulek defektu 7', 2);

-- Obrázky u defektů
INSERT INTO ear2018zs_34.public.picture (from_reporter, path, city_admin_id, defect) VALUES (TRUE, 'img/defect_1/2018-01-09_20-03-11-000.png', 1, 1);
INSERT INTO ear2018zs_34.public.picture (from_reporter, path, city_admin_id, defect) VALUES (TRUE, 'img/defect_1/2018-01-09_20-03-12-000.png', 1, 1);

-- Orgány
INSERT INTO ear2018zs_34.public.repair_service (email, name, phone) VALUES ('metrostav@whatever.cz', 'Metrostav a.s.', 123456789);
INSERT INTO ear2018zs_34.public.repair_service (email, name, phone) VALUES ('organ1@organmail.cz', 'Název orgánu 1', 987654321);
INSERT INTO ear2018zs_34.public.repair_service (email, name, phone) VALUES ('organ2@organmail.cz', 'Název orgánu 2', 321654987);

-- Orgány u defektů
INSERT INTO ear2018zs_34.public.defect_repair_service_list (defect_list_id, repair_service_list_id) VALUES (1, 1);
INSERT INTO ear2018zs_34.public.defect_repair_service_list (defect_list_id, repair_service_list_id) VALUES (1, 2);
INSERT INTO ear2018zs_34.public.defect_repair_service_list (defect_list_id, repair_service_list_id) VALUES (2, 2);
INSERT INTO ear2018zs_34.public.defect_repair_service_list (defect_list_id, repair_service_list_id) VALUES (2, 3);
INSERT INTO ear2018zs_34.public.defect_repair_service_list (defect_list_id, repair_service_list_id) VALUES (3, 3);
INSERT INTO ear2018zs_34.public.defect_repair_service_list (defect_list_id, repair_service_list_id) VALUES (4, 1);

-- Poznámky u defektů
INSERT INTO ear2018zs_34.public.note (inter, text, city_admin_id, defect) VALUES (FALSE, 'Text poznámky 1 u defektu 1', 1, 1);
INSERT INTO ear2018zs_34.public.note (inter, text, city_admin_id, defect) VALUES (TRUE, 'Text poznámky 2 u defektu 1', 1, 1);
INSERT INTO ear2018zs_34.public.note (inter, text, city_admin_id, defect) VALUES (TRUE, 'Text poznámky u defektu 2', 1, 2);
INSERT INTO ear2018zs_34.public.note (inter, text, city_admin_id, defect) VALUES (TRUE, 'Text poznámky u defektu 4', 1, 4);