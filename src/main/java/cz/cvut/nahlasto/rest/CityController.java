package cz.cvut.nahlasto.rest;

import cz.cvut.nahlasto.model.City;
import cz.cvut.nahlasto.model.RepairService;
import cz.cvut.nahlasto.rest.dto.ResponseDTO;
import cz.cvut.nahlasto.rest.util.CustomResponseEntity;
import cz.cvut.nahlasto.security.SecurityUtils;
import cz.cvut.nahlasto.service.CityService;
import cz.cvut.nahlasto.service.RepairServiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Represents a REST controller for the City entity.
 */
@RestController
@RequestMapping("/cities")
public class CityController extends AbstractController<City> {

    private final RepairServiceService repairServiceService;

    @Autowired
    public CityController(CityService cityService, RepairServiceService repairServiceService) {
        super(cityService, City.class, CityController.class);
        this.repairServiceService = repairServiceService;
    }

    /**
     * Adds a RepairService entity to City entity.
     * @param repairServiceId a RepairService entity id to add to City entity
     * @return a response entity containing a message about the execution
     */
    @RequestMapping(value = "/repair-services/{repairServiceId}", method = RequestMethod.PUT)
    @PreAuthorize("!anonymous")
    public ResponseEntity<ResponseDTO> addRepairServiceToCity(@PathVariable Integer repairServiceId) {
        City city = ((CityService) abstractService).findById(SecurityUtils.getCurrentUser().getCity().getId());
        RepairService repairService = repairServiceService.findById(repairServiceId);
        if (!((CityService) abstractService).addRepairService(city, repairService)) {
            return CustomResponseEntity.conflict("Repair servuce named '" + repairService.getName() +
                    "' (identifier: '" + repairServiceId + "') was already added to city with id: '" + city.getId() + "'");
        }

        return CustomResponseEntity.ok("Successfully added a repair service named '" + repairService.getName() +
                "' (identifier: '" + repairServiceId + "') to the city with identifier '" + city.getId() + "'");
    }

    /**
     * Removes a RepairService entity from City entity.
     * @param repairServiceId a RepairService entity id to remove from City entity
     * @return a response entity containing a message about the execution
     */
    @RequestMapping(value = "/repair-services/{repairServiceId}", method = RequestMethod.DELETE)
    @PreAuthorize("!anonymous")
    public ResponseEntity<ResponseDTO> removeRepairServiceFromCity(@PathVariable Integer repairServiceId) {
        City city = ((CityService) abstractService).findById(SecurityUtils.getCurrentUser().getCity().getId());
        RepairService repairService = repairServiceService.findById(repairServiceId);
        if (!((CityService) abstractService).removeRepairService(city, repairService))
            return CustomResponseEntity.notFound("Repair service named '" + repairService.getName() +
                    "' (identifier: '" + repairServiceId + "') was not found at city " +
                    "with identifier '" + city.getId() + "'");

        return CustomResponseEntity.ok("Successfully removed a repair service named '" + repairService.getName() +
                "' (identifier: '" + repairServiceId + "') from the city with identifier '" + city.getId() + "'");
    }

   /* @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseDTO> getStatistics(@PathVariable("cityToken") String cityToken) {     // TODO
        /*Statistics statistics = null;
        try {
            statistics = ((CityService) abstractService).getStatistic(cityToken);
        } catch (ResourceNotFoundException e) {
            logger.log(Level.SEVERE, e.getMessage());
        }

        return CustomResponseEntity.ok(statistics);
        return null;
    }*/

}
