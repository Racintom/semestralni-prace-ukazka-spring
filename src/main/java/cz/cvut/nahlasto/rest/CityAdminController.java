package cz.cvut.nahlasto.rest;

import cz.cvut.nahlasto.model.CityAdmin;
import cz.cvut.nahlasto.service.CityAdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Represents a REST controller for the CityAdmin entity.
 */
@RestController
@RequestMapping("/city-admins")
public class CityAdminController extends AbstractController<CityAdmin> {

    @Autowired
    protected CityAdminController(CityAdminService cityAdminService) {
        super(cityAdminService, CityAdmin.class, CityAdminController.class);
    }

}
