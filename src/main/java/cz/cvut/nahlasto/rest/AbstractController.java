package cz.cvut.nahlasto.rest;

import cz.cvut.nahlasto.exception.NotMatchingIdentifiersException;
import cz.cvut.nahlasto.exception.ValidationException;
import cz.cvut.nahlasto.model.AbstractEntity;
import cz.cvut.nahlasto.rest.dto.ResponseDTO;
import cz.cvut.nahlasto.rest.util.CustomResponseEntity;
import cz.cvut.nahlasto.rest.util.MiscRestUtils;
import cz.cvut.nahlasto.service.AbstractService;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.logging.Logger;

/**
 * Represents an abstract REST controller providing common operations.
 * @param <T> a type of an entity to work with
 */
public abstract class AbstractController<T extends AbstractEntity> {

    protected final Logger logger;

    protected final Class<T> type;
    protected final Class controllerClass;
    protected final AbstractService<T> abstractService;


    protected AbstractController(AbstractService<T> abstractService, Class<T> type, Class controllerClass) {
        this.abstractService = abstractService;
        this.type = type;
        this.controllerClass = controllerClass;
        this.logger = Logger.getLogger(controllerClass.getSimpleName());
    }

    /**
     * Returns a response entity containing an entity found by its id.
     * @param id id of an entity
     * @return a response entity containing an entity found by its id
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseDTO> findById(@PathVariable("id") Integer id) {
        return CustomResponseEntity.ok(abstractService.findById(id));
    }

    /**
     * Returns a response entity containing a list of all existing entities
     * @return a response entity containing a list of all existing entities
     */
    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseDTO> findAll() {
        return CustomResponseEntity.ok(abstractService.findAll());
    }

    /**
     * Returns a response entity containing a list of entities specified by offset and limit.
     * @param offset an offset
     * @param limit a limit
     * @return a response entity containing a list of entities specified by offset and limit
     */
    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, params = {"offset", "limit"})
    public ResponseEntity<ResponseDTO> findWithOffsetAndLimit(@RequestParam int offset,
                                                              @RequestParam int limit) {
        return CustomResponseEntity.ok(abstractService.findWithOffsetAndLimit(offset, limit));
    }

    /**
     * Creates a new entity and returns a response entity containing id of the newly created entity.
     * @param entity an entity to create
     * @param result a BindingResult object containing list of errors if any happened
     * @return a response entity containing id of the newly created entity
     */
    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseDTO> create(@Valid @RequestBody T entity, BindingResult result) {
        if (result.hasErrors())
            throw new ValidationException(MiscRestUtils.buildErrorList(result));
        abstractService.createWithAuthorization(entity);
        return CustomResponseEntity.created("Successfully created " + type.getSimpleName(), "/{id}", entity.getId());
    }

    /**
     * Updates an entity specified by its id.
     * @param id id of an entity
     * @param entity an entity to update
     * @param result a BindingResult object containing list of errors if any happened
     * @return a response entity containing a message about the execution
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseDTO> update(@PathVariable("id") Integer id, @Valid @RequestBody T entity, BindingResult result) {
        final T original = abstractService.findById(id);
        if (!original.getId().equals(entity.getId()))
            throw new NotMatchingIdentifiersException(type.getSimpleName(), original.getId(), entity.getId());
        if (result.hasErrors())
            throw new ValidationException(MiscRestUtils.buildErrorList(result));
        abstractService.updateWithAuthorization(entity);
        return CustomResponseEntity.ok("Successfully updated " + type.getSimpleName() + " with identifier '" + id + "'");
    }

    /**
     * Removes an entity specified by its id.
     * @param id id of an entity
     * @return a response entity containing a message about the execution
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<ResponseDTO> remove(@PathVariable("id") Integer id) {
        abstractService.remove(abstractService.findById(id));
        return CustomResponseEntity.ok("Successfully removed " + type.getSimpleName() + " with identifier '" + id + "'");
    }

}
