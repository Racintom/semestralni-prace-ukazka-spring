package cz.cvut.nahlasto.rest;

import cz.cvut.nahlasto.exception.ValidationException;
import cz.cvut.nahlasto.model.Defect;
import cz.cvut.nahlasto.model.Note;
import cz.cvut.nahlasto.model.Picture;
import cz.cvut.nahlasto.model.RepairService;
import cz.cvut.nahlasto.model.enums.DefectStatus;
import cz.cvut.nahlasto.rest.dto.ResponseDTO;
import cz.cvut.nahlasto.rest.dto.SingleObjectDTO;
import cz.cvut.nahlasto.rest.util.CustomResponseEntity;
import cz.cvut.nahlasto.rest.util.MiscRestUtils;
import cz.cvut.nahlasto.service.DefectService;
import cz.cvut.nahlasto.service.NoteService;
import cz.cvut.nahlasto.service.PictureService;
import cz.cvut.nahlasto.service.RepairServiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Represents a REST controller for the Defect entity.
 */
@RestController
@RequestMapping("/defects")
public class DefectController extends AbstractController<Defect> {

    private final PictureService pictureService;
    private final NoteService noteService;
    private final RepairServiceService repairServiceService;

    @Autowired
    public DefectController(DefectService defectService, PictureService pictureService, NoteService noteService, RepairServiceService repairServiceService) {
        super(defectService, Defect.class, DefectController.class);
        this.pictureService = pictureService;
        this.noteService = noteService;
        this.repairServiceService = repairServiceService;
    }

    /**
     * Creates a Defect entity. If a picture is provided, it will be saved to server's file system.
     * @param entity a Defect entity to create
     * @param result a BindingResult object containing list of errors if any happened
     * @return
     */
    @Override
    public ResponseEntity<ResponseDTO> create(@Valid @RequestBody Defect entity, BindingResult result) {
        if (result.hasErrors())
            throw new ValidationException(MiscRestUtils.buildErrorList(result));
        List<Picture> pictures = new ArrayList<>(entity.getPictureList());
        if (pictures.size() > 0) // Abychom mohli nejdříve uložit defekt, je potřeba od něj odebrat fotku, protože k ní
            entity.getPictureList().clear();  // zatím nebyla vygenerována cesta a při jejím persistu by došlo k výjimce
        ((DefectService) abstractService).create(entity);
        String message = "Successfully created Defect";

        // V tuto chvíli už je k dispozici id vytvořeného defektu a lze ho tak použít k vygenerování cesty k fotce
        if (pictures.size() > 0) {
            Picture picture = pictures.get(0);
            String pathToPicture = MiscRestUtils.generatePathToPicture(entity.getId());
            picture.setPath(pathToPicture);
            picture.setFromReporter(true);
            entity.addPicture(picture);
            ((DefectService)abstractService).update(entity);
            if (pictureService.savePicture(picture.getData(), entity.getId(), pathToPicture))
                message += " and saved the picture";
            else
                message += " but picture saving failed (probably bad picture data format)";
        }
        return CustomResponseEntity.created(message, "/{id}", entity.getId());
    }

    /**
     * Returns a response entity containing a list of all defects conforming to provided city token.
     * @param cityToken a city token to use in the search
     * @return a response entity containing a list of all defects conforming to provided city token
     */
    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, params = {"cityToken"})
    public ResponseEntity<ResponseDTO> getCityDefectList(@RequestParam String cityToken) {
        return CustomResponseEntity.ok(((DefectService) abstractService).findCityDefectsByToken(cityToken));
    }

    /**
     * Returns a response entity containing a list of all defects specified by defect status, offset and limit.
     * @param status defect status to search by
     * @param offset an offset
     * @param limit a limit
     * @return a response entity containing a list of all defects specified by defect status, offset and limit.
     */
    @RequestMapping(value = "/status", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE, params = {"offset", "limit"})
    public ResponseEntity<ResponseDTO> getCityDefectList(@RequestBody DefectStatus status,
                                                         @RequestParam int offset,
                                                         @RequestParam int limit) {
        List<Defect> defectList = ((DefectService) abstractService).findWithOffsetAndLimitAndStatus(offset, limit, status);
        return CustomResponseEntity.ok(defectList);
    }

    /**
     * Updates a defect's state specified by its id.
     * @param id id of a defect
     * @param newState a new state of the defect
     * @return a response entity containing a message about the execution
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE, params = {"newState"})
    public ResponseEntity<ResponseDTO> changeDefectState(@PathVariable("id") Integer id,
                                                         @RequestParam DefectStatus newState) {
        Defect defect = abstractService.findById(id);
        defect.setStatus(newState);
        defect.setDateModified(LocalDate.now());
        abstractService.updateWithAuthorization(defect);
        return CustomResponseEntity.ok("Successfully changed Defect's (id: '" + id + "') state to " + newState);
    }

    /**
     * Duplicates a defect specified by id.
     * @param defectId
     * @param duplicatedDefectId id of a defect to duplicate
     * @return a response entity containing a message about the execution
     */
    @RequestMapping(method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE, params = {"defectId", "duplicatedDefectId"})
    public ResponseEntity<ResponseDTO> duplicateDefect(@RequestParam Integer defectId,
                                                       @RequestParam Integer duplicatedDefectId) {
        Defect duplicatedDefect = abstractService.findById(duplicatedDefectId);
        duplicatedDefect.setStatus(DefectStatus.DUPLICATE);
        abstractService.updateWithAuthorization(duplicatedDefect);
        return CustomResponseEntity.ok("Successfully marked defect with identifier '" + duplicatedDefectId + "' as DUPLICATE");
    }

    /**
     * Adds a picture to a defect specified by id and saves it to local file system.
     * Accepts JSON with picture data encoded in Base64.
     * @param id defect id to add a picture to
     * @param picture a Picture entity containing picture data encoded in Base64
     * @return a response entity containing a message about the execution
     */
    @RequestMapping(value = "/{id}/pictures", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseDTO> addPictureToDefect(@PathVariable Integer id, @RequestBody Picture picture) {
        Defect defect = abstractService.findById(id);
        picture.setPath(MiscRestUtils.generatePathToPicture(id));
        if (!pictureService.savePicture(picture.getData(), id, picture.getPath()))
            return ResponseEntity.badRequest().body(new SingleObjectDTO<>(false, "Picture could not be added to the defect (probably bad picture data format)", null));
        defect.addPicture(picture);
        defect.setDateModified(LocalDate.now());
        abstractService.updateWithAuthorization(defect);
        return CustomResponseEntity.ok("Successfully added a picture to the defect with identifier '" + id + "'");
    }

    /**
     * Adds a picture to a defect specified by id and saves it to local file system.
     * Accepts picture data as form data.
     * @param id defect id to add a picture to
     * @param file picture data as form data
     * @return a response entity containing a message about the execution
     * @throws IOException
     */
    @RequestMapping(value = "/{id}/pictures", method = RequestMethod.POST)
    public ResponseEntity<ResponseDTO> addPictureToDefect(@PathVariable Integer id, @RequestParam("file") MultipartFile file) throws IOException {
        if (file == null)
            throw new ValidationException("File is missing.");
        Defect defect = abstractService.findById(id);
        Picture picture = new Picture(MiscRestUtils.generatePathToPicture(id), false, null, null, null);
        if (!pictureService.savePicture(file.getBytes(), id, picture.getPath()))
            return ResponseEntity.badRequest().body(new SingleObjectDTO<>(false, "Picture could not be added to the defect (probably bad picture data format)", null));
        defect.addPicture(picture);
        defect.setDateModified(LocalDate.now());
        abstractService.updateWithAuthorization(defect);
        return CustomResponseEntity.ok("Successfully added a picture to the defect with identifier '" + id + "'");
    }

    /**
     * Removes a picture specified by picture id from a defect specified by defect id.
     * @param defectId id of the defect to remove a picture from
     * @param pictureId id of the deleted picture
     * @return a response entity containing a message about the execution
     */
    @RequestMapping(value = "/{defectId}/pictures/{pictureId}", method = RequestMethod.DELETE)
    public ResponseEntity<ResponseDTO> removePictureFromDefect(@PathVariable Integer defectId,
                                                               @PathVariable Integer pictureId) {
        Defect defect = abstractService.findById(defectId);
        Picture picture = pictureService.findById(pictureId);
        if (!defect.removePicture(picture))
            return CustomResponseEntity.notFound("Picture with identifier '" + pictureId +
                    "' was not found at defect with identifier '" + defectId + "'");
        defect.setDateModified(LocalDate.now());
        abstractService.updateWithAuthorization(defect);
        pictureService.remove(picture);
        pictureService.deletePicture(picture.getPath());
        return CustomResponseEntity.ok("Successfully removed a picture with identifier '" + pictureId +
                "' from the defect with identifier '" + defectId + "'");
    }

    /**
     * Adds a note to a defect specified by its id.
     * @param id id of the defect to add a note to
     * @param note a note to add
     * @param result a BindingResult object containing list of errors if any happened
     * @return a response entity containing a message about the execution
     */
    @RequestMapping(value = "/{id}/notes", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseDTO> addNoteToDefect(@PathVariable Integer id, @Valid @RequestBody Note note, BindingResult result) {
        if (result.hasErrors())
            throw new ValidationException(MiscRestUtils.buildErrorList(result));
        Defect defect = abstractService.findById(id);
        defect.addNote(note);
        defect.setDateModified(LocalDate.now());
        abstractService.updateWithAuthorization(defect);
        return CustomResponseEntity.ok("Successfully added a note to the defect with identifier '" + id + "'");
    }

    /**
     * Removes a note specified by note id from a defect specified by defect id.
     * @param defectId id of the defect to remove a note from
     * @param noteId id of the removed note
     * @return a response entity containing a message about the execution
     */
    @RequestMapping(value = "/{defectId}/notes/{noteId}", method = RequestMethod.DELETE)
    public ResponseEntity<ResponseDTO> removeNoteFromDefect(@PathVariable Integer defectId,
                                                            @PathVariable Integer noteId) {
        Defect defect = abstractService.findById(defectId);
        Note note = noteService.findById(noteId);
        if (!defect.removeNote(note))
            return CustomResponseEntity.notFound("Note with identifier '" + noteId +
                    "' was not found at defect with identifier '" + defectId + "'");
        defect.setDateModified(LocalDate.now());
        abstractService.updateWithAuthorization(defect);
        noteService.remove(note);
        return CustomResponseEntity.ok("Successfully removed a note with identifier '" + noteId +
                "' from the defect with identifier '" + defectId + "'");
    }

    /**
     * Adds an existing repair service specified by repair service id to a defect specified by defect id.
     * @param defectId id of the defect to add a repair service to
     * @param repairServiceId an existing repair service id to add
     * @return a response entity containing a message about the execution
     */
    @RequestMapping(value = "/{defectId}/repair-services/{repairServiceId}", method = RequestMethod.PUT)
    public ResponseEntity<ResponseDTO> addRepairServiceToDefect(@PathVariable Integer defectId,
                                                                @PathVariable Integer repairServiceId) {
        Defect defect = abstractService.findById(defectId);
        RepairService repairService = repairServiceService.findById(repairServiceId);
        defect.addRepairService(repairService);
        defect.setDateModified(LocalDate.now());
        abstractService.updateWithAuthorization(defect);
        return CustomResponseEntity.ok("Successfully added a repair service named '" + repairService.getName() +
                "' (identifier: '" + repairServiceId + "') to the defect with identifier '" + defectId + "'");
    }

    /**
     * Removes a repair service specified by repair service id from a defect specified by defect id.
     * @param defectId id of the defect to remove a repair service from
     * @param repairServiceId id of the removed repair service
     * @return a response entity containing a message about the execution
     */
    @RequestMapping(value = "/{defectId}/repair-services/{repairServiceId}", method = RequestMethod.DELETE)
    public ResponseEntity<ResponseDTO> removeRepairServiceFromDefect(@PathVariable Integer defectId,
                                                                     @PathVariable Integer repairServiceId) {
        Defect defect = abstractService.findById(defectId);
        RepairService repairService = repairServiceService.findById(repairServiceId);
        if (!defect.removeRepairService(repairService))
            return CustomResponseEntity.notFound("Repair service named '" + repairService.getName() +
                    "' (identifier: '" + repairServiceId + "') was not found at defect " +
                    "with identifier '" + defectId + "'");
        defect.setDateModified(LocalDate.now());
        abstractService.updateWithAuthorization(defect);
        return CustomResponseEntity.ok("Successfully removed a repair service named '" + repairService.getName() +
                "' (identifier: '" + repairServiceId + "') from the defect with identifier '" + defectId + "'");
    }
}
