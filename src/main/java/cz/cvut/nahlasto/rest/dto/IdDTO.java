package cz.cvut.nahlasto.rest.dto;

/**
 * A concrete representation of response Data Transfer Object containing additional id of an entity.
 */
public class IdDTO extends ResponseDTO {

    private Integer id;

    public IdDTO(boolean success, String message, Integer id) {
        super(success, message);
        this.id = id;
    }

    public Integer getId() {
        return id;
    }
}
