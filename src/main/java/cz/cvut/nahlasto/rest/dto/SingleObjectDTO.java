package cz.cvut.nahlasto.rest.dto;

import cz.cvut.nahlasto.model.AbstractEntity;

/**
 * A concrete representation of response Data Transfer Object containing additional data.
 */
public class SingleObjectDTO<T extends AbstractEntity> extends ResponseDTO {

    private T data;

    public SingleObjectDTO(boolean success, String message, T data) {
        super(success, message);
        this.data = data;
    }

    public T getData() {
        return data;
    }
}
