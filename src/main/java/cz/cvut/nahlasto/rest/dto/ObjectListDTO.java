package cz.cvut.nahlasto.rest.dto;

import cz.cvut.nahlasto.model.AbstractEntity;

import java.util.List;

/**
 * A concrete representation of response Data Transfer Object containing additional list of data.
 */
public class ObjectListDTO<T extends AbstractEntity> extends ResponseDTO {

    private List<T> data;

    public ObjectListDTO(boolean success, String message, List<T> data) {
        super(success, message);
        this.data = data;
    }

    public List<T> getData() {
        return data;
    }
}
