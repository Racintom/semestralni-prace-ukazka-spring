package cz.cvut.nahlasto.rest.dto;

/**
 * An abstract response Data Transfer Object containing common data.
 */
public abstract class ResponseDTO {

    private boolean success;
    private String message;

    protected ResponseDTO(boolean success, String message) {
        this.success = success;
        this.message = message;
    }

    public boolean isSuccess() {
        return success;
    }

    public String getMessage() {
        return message;
    }
}
