package cz.cvut.nahlasto.rest;

import cz.cvut.nahlasto.model.Picture;
import cz.cvut.nahlasto.service.PictureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Represents a REST controller for the Picture entity.
 */
@RestController
@RequestMapping("/pictures")
public class PictureController extends AbstractController<Picture> {

    @Autowired
    public PictureController(PictureService pictureService) {
        super(pictureService, Picture.class, PictureController.class);
    }

}
