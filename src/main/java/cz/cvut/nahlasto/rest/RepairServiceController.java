package cz.cvut.nahlasto.rest;

import cz.cvut.nahlasto.model.RepairService;
import cz.cvut.nahlasto.security.SecurityUtils;
import cz.cvut.nahlasto.service.RepairServiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Represents a REST controller for the RepairService entity.
 */
@RestController
@RequestMapping("/repair-services")
public class RepairServiceController extends AbstractController<RepairService> {

    @Autowired
    public RepairServiceController(RepairServiceService repairServiceService) {
        super(repairServiceService, RepairService.class, RepairServiceController.class);
    }

    /**
     * Returns whether the repair service specified by its id is assigned to any city, or not.
     * @param repairServiceId id of the repair service
     * @return <code>true</code> if the repair service is assigned to any city; <code>false</code> otherwise
     */
    @RequestMapping(method = RequestMethod.GET, value = "/cities/{repairServiceId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Boolean isAssignedToCity(@PathVariable Integer repairServiceId) {

        RepairService repairService = abstractService.findById(repairServiceId);
        return repairService.getCityList().contains(SecurityUtils.getCurrentUser().getCity());
    }


}
