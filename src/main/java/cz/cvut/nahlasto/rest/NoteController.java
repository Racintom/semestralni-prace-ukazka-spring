package cz.cvut.nahlasto.rest;

import cz.cvut.nahlasto.model.Note;
import cz.cvut.nahlasto.service.NoteService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import cz.cvut.nahlasto.rest.dto.ResponseDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Represents a REST controller for the Note entity.
 */
@RestController
@RequestMapping("/notes")
public class NoteController extends AbstractController<Note> {

    @Autowired
    public NoteController(NoteService noteService) {
        super(noteService, Note.class, NoteController.class);
    }

    @RequestMapping(value = "/user", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, params = {"userToken", "offset", "limit"})
    public ResponseEntity<ResponseDTO> getUserNoteList(@RequestParam String userToken,
            @RequestParam int offset,
            @RequestParam int limit) {
    return null;
    }

    @RequestMapping(value = "/user", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, params = {"userId", "offset", "limit"})
    public ResponseEntity<ResponseDTO> getUserNoteList(@RequestParam Integer userId,
            @RequestParam int offset,
            @RequestParam int limit) {
        return null;}

    @RequestMapping(value = "/user", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, params = {"offset", "limit"})
    public ResponseEntity<ResponseDTO> getUserNoteList(@RequestParam int offset,
            @RequestParam int limit) {

        return null;}

    @RequestMapping(value = "/defect",method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, params = {"defectToken", "offset", "limit"})
    public ResponseEntity<ResponseDTO> getDefectNoteList(@RequestParam String defectToken,
            @RequestParam int offset,
            @RequestParam int limit) {
        return null;
    }

    @RequestMapping(value = "/defect", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, params = {"defectId", "offset", "limit"})
    public ResponseEntity<ResponseDTO> getDefectNoteList(@RequestParam Integer defectId,
            @RequestParam int offset,
            @RequestParam int limit) {
        return null;
    }

    @RequestMapping(value = "/defect", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, params = {"offset", "limit"})
    public ResponseEntity<ResponseDTO> getDefectNoteList(@RequestParam int offset,
            @RequestParam int limit) {
        return null;
    }

}
