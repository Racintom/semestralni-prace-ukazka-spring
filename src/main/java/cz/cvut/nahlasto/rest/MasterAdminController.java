package cz.cvut.nahlasto.rest;

import cz.cvut.nahlasto.model.MasterAdmin;
import cz.cvut.nahlasto.service.MasterAdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Represents a REST controller for the MasterAdmin entity.
 */
@RestController
@RequestMapping("/master-admins")
public class MasterAdminController extends AbstractController<MasterAdmin> {

    @Autowired
    protected MasterAdminController(MasterAdminService masterAdminService) {
        super(masterAdminService, MasterAdmin.class, MasterAdminController.class);

    }
}
