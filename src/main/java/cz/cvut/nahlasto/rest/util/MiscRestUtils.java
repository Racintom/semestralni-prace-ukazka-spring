package cz.cvut.nahlasto.rest.util;

import org.springframework.http.HttpHeaders;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * A utility class providing miscellaneous utilities for REST layer.
 */
public final class MiscRestUtils {

    /**
     * Returns a string containing a list of errors.
     * @param errors an Errors object containing errors to build a list from
     * @return string containing a list of errors
     */
    public static String buildErrorList(Errors errors) {
        StringBuilder sb = new StringBuilder();
        for (FieldError fe : errors.getFieldErrors()) {
            sb.append(" Field '").append(fe.getField()).append("' must not be '").append(fe.getRejectedValue()).append("'.");
        }
        return sb.toString();
    }

    /**
     * Creates and returns HttpHeaders object containing Location header.
     * @param path a path to create a Location header from
     * @param id an id to create a Location header from
     * @return HttpHeaders object containing Location header
     */
    public static HttpHeaders createLocationHeader(String path, Integer id) {
        URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path(path).buildAndExpand(id).toUri();
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(uri);
        return headers;
    }

    /**
     * Generates and returns a string path to picture.
     * @param defectId id of a defect the picture is assigned to
     * @return a string path to picture
     */
    public static String generatePathToPicture(Integer defectId) {
        return Constants.PATH_TO_DEF_IMG_DIR_PREFIX + defectId + "/" +
                (new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss-SSS'.png'").format(new Date()));
    }
}
