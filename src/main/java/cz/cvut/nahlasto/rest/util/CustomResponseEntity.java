package cz.cvut.nahlasto.rest.util;

import cz.cvut.nahlasto.model.AbstractEntity;
import cz.cvut.nahlasto.rest.dto.IdDTO;
import cz.cvut.nahlasto.rest.dto.ObjectListDTO;
import cz.cvut.nahlasto.rest.dto.ResponseDTO;
import cz.cvut.nahlasto.rest.dto.SingleObjectDTO;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;

/**
 * Class providing static methods for creating custom response entities.
 */
public final class CustomResponseEntity {

    public static <T extends AbstractEntity> ResponseEntity<ResponseDTO> ok(T result) {
        return new ResponseEntity<>(new SingleObjectDTO<>(true, null, result), HttpStatus.OK);
    }

    public static <T extends AbstractEntity> ResponseEntity<ResponseDTO> ok(List<T> result) {
        return new ResponseEntity<>(new ObjectListDTO<>(true, null, result), HttpStatus.OK);
    }

    public static ResponseEntity<ResponseDTO> ok(String message) {
        return new ResponseEntity<>(new SingleObjectDTO<>(true, message, null), HttpStatus.OK);
    }

    public static ResponseEntity<ResponseDTO> created(String message, String path, Integer id) {
        HttpHeaders headers = MiscRestUtils.createLocationHeader(path, id);
        return new ResponseEntity<>(new IdDTO(true, message, id), headers, HttpStatus.CREATED);
    }

    public static ResponseEntity<ResponseDTO> notFound(String message) {
        return new ResponseEntity<>(new SingleObjectDTO<>(false, message, null), HttpStatus.NOT_FOUND);
    }

    public static ResponseEntity<ResponseDTO> conflict(String message) {
        return new ResponseEntity<>(new SingleObjectDTO<>(false, message, null), HttpStatus.CONFLICT);
    }

    public static ResponseEntity<Object> unprocessableEntity(String message) {
        return new ResponseEntity<>(new SingleObjectDTO<>(false, message, null), HttpStatus.UNPROCESSABLE_ENTITY);
    }

    public static ResponseEntity<Object> badRequest(String message) {
        return new ResponseEntity<>(new SingleObjectDTO<>(false, message, null), HttpStatus.BAD_REQUEST);
    }

    public static ResponseEntity<Object> methodNotAllowed(String message) {
        return new ResponseEntity<>(new SingleObjectDTO<>(false, message, null), HttpStatus.METHOD_NOT_ALLOWED);
    }

    public static ResponseEntity<ResponseDTO> error(String message, HttpStatus httpStatus) {
        return new ResponseEntity<>(new SingleObjectDTO<>(false, message, null), httpStatus);
    }

}
