package cz.cvut.nahlasto.rest.util;

/**
 * Class for constants used for generating picture paths.
 */
public final class Constants {

    private Constants() {}

    public static final String IMG_DIR = "img";
    public static final String DEF_IMG_DIR_PREFIX = "defect_";
    public static final String PATH_TO_DEF_IMG_DIR_PREFIX = IMG_DIR + "/" + DEF_IMG_DIR_PREFIX;

}
