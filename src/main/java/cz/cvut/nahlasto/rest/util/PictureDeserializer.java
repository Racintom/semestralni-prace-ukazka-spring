package cz.cvut.nahlasto.rest.util;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import cz.cvut.nahlasto.exception.ValidationException;
import cz.cvut.nahlasto.model.Picture;

import java.io.IOException;

/**
 * Class for deserializing Picture entity coming to server in JSON format into object.
 */
public class PictureDeserializer extends StdDeserializer<Picture> {

    protected PictureDeserializer() {
        this(Picture.class);
    }

    protected PictureDeserializer(Class<?> vc) {
        super(vc);
    }

    @Override
    public Picture deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        JsonNode node = jsonParser.getCodec().readTree(jsonParser);
        JsonNode pictureDataNode = node.get("picture");
        if (pictureDataNode == null)
            throw new ValidationException("Attribute 'picture' containing picture data is missing or null.");
        return new Picture(null, false, null, null, pictureDataNode.textValue());
    }
}
