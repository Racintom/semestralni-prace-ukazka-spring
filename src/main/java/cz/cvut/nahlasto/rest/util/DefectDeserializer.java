package cz.cvut.nahlasto.rest.util;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import cz.cvut.nahlasto.dao.CityDao;
import cz.cvut.nahlasto.dao.DefectDao;
import cz.cvut.nahlasto.exception.ResourceNotFoundException;
import cz.cvut.nahlasto.model.Address;
import cz.cvut.nahlasto.model.Defect;
import cz.cvut.nahlasto.model.LocationWGS;
import cz.cvut.nahlasto.model.Picture;
import cz.cvut.nahlasto.model.enums.LocatedUsing;
import cz.cvut.nahlasto.service.CityService;
import cz.cvut.nahlasto.service.DefectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.time.LocalDate;

/**
 * Class for deserializing Defect entity coming to server in JSON format into object.
 */
@Component
public class DefectDeserializer extends StdDeserializer<Defect> {

    private DefectService defectService;
    private CityService cityService;

    protected DefectDeserializer(Class<?> vc) {
        super(vc);
    }

    @Autowired
    public DefectDeserializer(DefectService defectService, CityService cityService) {
        super(Defect.class);
        this.defectService = defectService;
        this.cityService = cityService;
    }

    @Override
    public Defect deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        JsonNode node = jsonParser.getCodec().readTree(jsonParser);
        JsonNode idNode = node.get("id");

        // Updating defect
        if (idNode != null) {
            Defect original = defectService.findById(idNode.intValue());
            return new Defect(
                    idNode.intValue(),
                    node.get("title")       == null ? null : node.get("title").textValue(),
                    node.get("description") == null ? null : node.get("description").textValue(),
                    original.getDateCreated(),
                    LocalDate.now(),
                    node.get("reporterEmail") == null ? original.getReporterEmail() : node.get("reporterEmail").textValue(),
                    original.getStatus(),
                    original.getLocatedUsing(),
                    node.get("manualLocation") == null ? original.getManualLocation() : node.get("manualLocation").textValue(),
                    original.getLocationWGS(),
                    original.getAddress(),
                    original.getCity(),
                    original.getPictureList(),
                    original.getNoteList(),
                    original.getRepairServiceList()
            );
        }
        // Creating defect
        else {
            Defect defect = new Defect();
            JsonNode cityIdNode = node.get("cityId");
            if (cityIdNode != null)
                defect.setCity(cityService.findById(cityIdNode.intValue()));
            defect.setTitle(node.get("title")                 == null ? "Default defect title" : node.get("title").textValue());
            defect.setDescription(node.get("description")     == null ? null : node.get("description").textValue());
            defect.setDateCreated(node.get("dateCreated")     == null ? LocalDate.now() : parseLocalDate(node.get("dateCreated").textValue()));
            defect.setReporterEmail(node.get("reporterEmail") == null ? null : node.get("reporterEmail").textValue());
            JsonNode locatedUsingNode = node.get("locatedUsing");
            JsonNode locationNode = node.get("locationWGS");
            JsonNode addressNode = node.get("address");
            JsonNode pictureNode = node.get("picture");
            try {
                defect.setLocatedUsing(LocatedUsing.valueOf(locatedUsingNode.textValue()));
            } catch (IllegalArgumentException ex) {
                throw new IllegalArgumentException("Enum error");
            }
            defect.setManualLocation(node.get("manualLocation") == null ? null : node.get("manualLocation").textValue());
            if (locationNode != null)
                defect.setLocationWGS(new LocationWGS(
                        locationNode.get("lat")  == null ? null : locationNode.get("lat").doubleValue(),
                        locationNode.get("lon")  == null ? null : locationNode.get("lon").doubleValue()
                ));
            if (addressNode != null)
                defect.setAddress(new Address(
                        addressNode.get("houseNumber") == null ? null : addressNode.get("houseNumber").textValue(),
                        addressNode.get("road")        == null ? null : addressNode.get("road").textValue(),
                        addressNode.get("suburb")      == null ? null : addressNode.get("suburb").textValue(),
                        addressNode.get("city")        == null ? null : addressNode.get("city").textValue(),
                        addressNode.get("postcode")    == null ? null : addressNode.get("postcode").textValue()

                ));
            if (pictureNode != null)
                defect.addPicture(new Picture("", true, null, null, pictureNode.textValue()));
            return defect;
        }
    }


    private LocalDate parseLocalDate(String localDateString) {
        String[] date = localDateString.split("-");
        return LocalDate.of(Integer.parseInt(date[0]), Integer.parseInt(date[1]), Integer.parseInt(date[2]));
    }
}
