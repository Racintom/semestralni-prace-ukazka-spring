package cz.cvut.nahlasto.rest.exceptionhandling;

import cz.cvut.nahlasto.exception.*;
import cz.cvut.nahlasto.rest.dto.ResponseDTO;
import cz.cvut.nahlasto.rest.util.CustomResponseEntity;
import org.springframework.beans.TypeMismatchException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolationException;
import java.io.IOException;

/**
 * Exception handler for processing different types of exceptions emerging on REST layer.
 */
@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = { ResourceNotFoundException.class })
    protected ResponseEntity<ResponseDTO> handleResourceNotFound(ResourceNotFoundException ex) {
        return CustomResponseEntity.notFound(ex.getMessage());
    }

    @ExceptionHandler(value = { ResourceAlreadyExistsException.class })
    protected ResponseEntity<ResponseDTO> handleResourceAlreadyExists(ResourceAlreadyExistsException ex) {
        return CustomResponseEntity.conflict(ex.getMessage());
    }

    @ExceptionHandler(value = { ValidationException.class, NotMatchingIdentifiersException.class })
    protected ResponseEntity<Object> handleValidation(NahlasToException ex) {
        return CustomResponseEntity.unprocessableEntity(ex.getMessage());
    }

    @ExceptionHandler(value = { ConstraintViolationException.class, IOException.class, IllegalArgumentException.class })
    protected ResponseEntity<Object> handleConstraintViolation(Exception ex) {
        return CustomResponseEntity.badRequest(ex.getMessage());
    }

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        return CustomResponseEntity.badRequest("The request body is malformed. Cause: " + ex.getCause().getMessage().split("[\r\n]")[0]);
    }

    @Override
    protected ResponseEntity<Object> handleHttpRequestMethodNotSupported(HttpRequestMethodNotSupportedException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        return CustomResponseEntity.methodNotAllowed(ex.getMessage());
    }

    @Override
    protected ResponseEntity<Object> handleTypeMismatch(TypeMismatchException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        return CustomResponseEntity.badRequest("A type mismatch happened. Cause: " + ex.getCause().getMessage());
    }

    @Override
    protected ResponseEntity<Object> handleMissingServletRequestParameter(MissingServletRequestParameterException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        return CustomResponseEntity.unprocessableEntity("Missing request parameter(s). Cause: " + ex.getCause().getMessage());
    }
}
