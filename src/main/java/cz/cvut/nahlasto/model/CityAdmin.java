package cz.cvut.nahlasto.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.util.List;

@SuppressWarnings("JpaQlInspection")
@Entity
@Table(name = "CityAdmin")
@NamedQuery(name = "CityAdmin.findByUsername", query = "SELECT c FROM CityAdmin c WHERE c.userName = :userName")
public class CityAdmin extends Admin {

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JsonIgnoreProperties({ "defectList" })
    private City city;

    @OneToMany(mappedBy = "cityAdmin", cascade = CascadeType.ALL)
    private List<Picture> pictureList;

    @OneToMany(mappedBy = "cityAdmin", cascade = CascadeType.ALL)
    private List<Note> noteList;

    public CityAdmin() {
    }

    public CityAdmin(City city, List<Picture> pictureList, List<Note> noteList) {
        this.city = city;
        this.pictureList = pictureList;
        this.noteList = noteList;
    }

    public void addPicture(Picture picture) {
        pictureList.add(picture);
        picture.setCityAdmin(this);
    }

    public void addNote(Note note) {
        noteList.add(note);
        note.setCityAdmin(this);
    }

    public City getCity() {
        return city;
    }

    public List<Picture> getPictureList() {
        return pictureList;
    }

    public List<Note> getNoteList() {
        return noteList;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public void setPictureList(List<Picture> pictureList) {
        this.pictureList = pictureList;
    }

    public void setNoteList(List<Note> noteList) {
        this.noteList = noteList;
    }

    @Override
    public String toString() {
        return "CityAdmin{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", userName='" + userName + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
