package cz.cvut.nahlasto.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
public class RepairService extends AbstractEntity {

    @NotNull
    @Basic
    @Column(nullable = false)
    private String name;

    @NotNull
    @Email
    @Basic
    @Column(nullable = false)
    private String email;

    @NotNull
    @Basic
    @Column(nullable = false)
    private String phone;

    @ManyToMany(mappedBy = "repairServiceList", cascade = CascadeType.PERSIST)
    @JsonIgnoreProperties({"repairServiceList"})
    private List<Defect> defectList;

    @ManyToMany(mappedBy = "repairServiceList", cascade = CascadeType.PERSIST)
    @JsonIgnoreProperties({"repairServiceList"})
    private List<City> cityList;

    @PreRemove
    private void removeRepairServiceFromDefects() {
        for (Defect d : defectList) {
            d.getRepairServiceList().remove(this);
        }
        for (City c : cityList) {
            c.getRepairServiceList().remove(this);
        }
    }

    public RepairService() {
    }

    public RepairService(String name, String email, String phone, List<Defect> defectList, List<City> cityList) {
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.defectList = defectList;
        this.cityList = cityList;
    }

    public void addDefect(Defect defect) {
        defectList.add(defect);
    }

    public void removeDefect(Defect defect) {
        defectList.remove(defect);
    }

    public void addDefectList(List<Defect> defectList) {
        this.defectList.addAll(defectList);
    }

    public void addCity(City city) {
        this.cityList.add(city);
    }

    public void removeCity(City city) {
        cityList.remove(city);
    }

    public void addCityList(List<City> cityList) {
        this.cityList.addAll(cityList);
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    public List<Defect> getDefectList() {
        return defectList;
    }

    public List<City> getCityList() {
        return cityList;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

}
