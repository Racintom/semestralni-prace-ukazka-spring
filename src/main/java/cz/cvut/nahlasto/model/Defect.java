package cz.cvut.nahlasto.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import cz.cvut.nahlasto.model.enums.DefectStatus;
import cz.cvut.nahlasto.model.enums.LocatedUsing;
import cz.cvut.nahlasto.rest.util.DefectDeserializer;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
@NamedQueries({
        @NamedQuery(name = "Defect.findAllByTitle",
                query = "SELECT d FROM Defect d WHERE d.title = :title"),
        @NamedQuery(name = "Defect.findAllByStatus",
                query = "SELECT d FROM Defect d WHERE d.status = :status"),
        @NamedQuery(name = "Defect.findAllDefectsForUser",
                query = "SELECT d FROM Defect d WHERE d.status = cz.cvut.nahlasto.model.enums.DefectStatus.APPROVED OR " +
                        "d.status = cz.cvut.nahlasto.model.enums.DefectStatus.IN_PROGRESS OR " +
                        "d.status = cz.cvut.nahlasto.model.enums.DefectStatus.COMPLETED"),
        @NamedQuery(name = "Defect.countAllDefect",
                query = "SELECT Count(d) FROM Defect d WHERE d.status = cz.cvut.nahlasto.model.enums.DefectStatus.APPROVED OR " +
                        "d.status = cz.cvut.nahlasto.model.enums.DefectStatus.IN_PROGRESS OR " +
                        "d.status = cz.cvut.nahlasto.model.enums.DefectStatus.WAITING"),
        @NamedQuery(name = "Defect.countAllRepairedDefect",
                query = "SELECT Count(d) FROM Defect d WHERE d.status = cz.cvut.nahlasto.model.enums.DefectStatus.COMPLETED"),
        @NamedQuery(name = "Defect.countAllDefectFrom",
                query = "SELECT Count(d) FROM Defect d WHERE (d.status = cz.cvut.nahlasto.model.enums.DefectStatus.APPROVED OR " +
                        "d.status = cz.cvut.nahlasto.model.enums.DefectStatus.IN_PROGRESS OR " +
                        "d.status = cz.cvut.nahlasto.model.enums.DefectStatus.WAITING) AND " +
                        "d.dateCreated > : date"),
        @NamedQuery(name = "Defect.countAllRepairedDefectFrom",
                query = "SELECT Count(d) FROM Defect d WHERE d.status = cz.cvut.nahlasto.model.enums.DefectStatus.COMPLETED AND " +
                        "d.dateCreated > :date"),
        @NamedQuery(name = "Defect.findCityDefectsByCityToken",
                query = "SELECT d FROM Defect d WHERE d.city.token LIKE :cityToken")
        /*,
    @NamedQuery(name = "Defect.findAllDefectsInRange",
    @NamedQuery(name = "Defect.findAllByTitle",
            query = "SELECT d FROM Defect d WHERE d.title = :title"),
    @NamedQuery(name = "Defect.findAllByStatus",
            query = "SELECT d FROM Defect d WHERE d.status = :status"),
    @NamedQuery(name = "Defect.findAllDefectsForUser",
            query = "SELECT d FROM Defect d WHERE d.status = cz.cvut.nahlasto.model.enums.DefectStatus.APPROVED OR " +
                    "d.status = cz.cvut.nahlasto.model.enums.DefectStatus.IN_PROGRESS OR " +
                    "d.status = cz.cvut.nahlasto.model.enums.DefectStatus.COMPLETED"),

    @NamedQuery(name = "Defect.findCityDefectsByCityId",
            query = "SELECT d FROM Defect d WHERE city.id = :cityId")
    /*, @NamedQuery(name = "Defect.findAllDefectsInRange",
            query = "SELECT d FROM Defect d LIMIT :fr,:to")*/})
@Table(name = "Defect")
@JsonDeserialize(using = DefectDeserializer.class)
public class Defect extends AbstractEntity {

    @NotNull
    @Column(nullable = false)
    private String title;

    @Column
    private String description;

    @NotNull
    @Column(nullable = false)
    private LocalDate dateCreated;

    @Column
    private LocalDate dateModified;

    @Email
    @Column
    private String reporterEmail;

    @NotNull
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private DefectStatus status = DefectStatus.WAITING;

    @NotNull
    @Enumerated(EnumType.STRING)
    private LocatedUsing locatedUsing;

    private String manualLocation;

    @Embedded
    private LocationWGS locationWGS;

    @Embedded
    private Address address;

    @NotNull
    @ManyToOne(cascade = CascadeType.PERSIST)
    @JsonIgnoreProperties({ "defectList" })
    private City city;

    // pry by se ten cascade type all ale nemel pouzivat (nikdy jsem nepochopil proc)
    @OneToMany(mappedBy = "defect", cascade = CascadeType.ALL)
    @JsonIgnoreProperties({ "defect", "cityAdmin" })
    private List<Picture> pictureList;

    @OneToMany(mappedBy = "defect", cascade = CascadeType.ALL)
    @JsonIgnoreProperties({ "defect", "cityAdmin" })
    private List<Note> noteList;

    @ManyToMany(cascade = CascadeType.PERSIST)
    @JsonIgnoreProperties({ "defectList", "cityList" })
    private List<RepairService> repairServiceList;

    public Defect() {
    }

    public Defect(String title, String description, LocalDate dateCreated, String reporterEmail, DefectStatus status, LocationWGS locationWGS,
                  Address address, City city, List<Picture> pictureList, List<Note> noteList, List<RepairService> repairServiceList) {
        this.title = title;
        this.description = description;
        this.dateCreated = dateCreated;
        this.reporterEmail = reporterEmail;
        this.status = status;
        this.locationWGS = locationWGS;
        this.address = address;
        this.city = city;
        this.pictureList = pictureList;
        this.noteList = noteList;
        this.repairServiceList = repairServiceList;
    }

    public Defect(int id, String title, String description, LocalDate dateCreated, LocalDate dateModified, String reporterEmail,
                  DefectStatus status, LocatedUsing locatedUsing, String manualLocation, LocationWGS locationWGS, Address address,
                  City city, List<Picture> pictureList, List<Note> noteList, List<RepairService> repairServiceList) {
        setId(id);
        this.title = title;
        this.description = description;
        this.dateCreated = dateCreated;
        this.dateModified = dateModified;
        this.reporterEmail = reporterEmail;
        this.status = status;
        this.locatedUsing = locatedUsing;
        this.manualLocation = manualLocation;
        this.locationWGS = locationWGS;
        this.address = address;
        this.city = city;
        this.pictureList = pictureList;
        this.noteList = noteList;
        this.repairServiceList = repairServiceList;
    }

    public void addPicture(Picture picture) {
        if (pictureList == null)
            pictureList = new ArrayList<>();
        pictureList.add(picture);
        picture.setDefect(this);
    }

    public boolean removePicture(Picture picture) {
        if (pictureList != null) {
            if (pictureList.remove(picture)) {
                picture.setDefect(null);
                return true;
            }
            return false;
        }
        return false;
    }


    public void addNote(Note note) {
        if (noteList == null)
            noteList = new ArrayList<>();
        noteList.add(note);
        note.setDefect(this);
    }

    public boolean removeNote(Note note) {
        if (noteList != null) {
            if (noteList.remove(note)) {
                note.setDefect(null);
                return true;
            }
            return false;
        }
        return false;
    }

    public void addRepairService(RepairService repairService) {
        if (repairServiceList == null)
            repairServiceList = new ArrayList<>();
        repairServiceList.add(repairService);
        repairService.addDefect(this);
    }

    public boolean removeRepairService(RepairService repairService) {
        if (repairServiceList != null) {
            if (repairServiceList.remove(repairService)) {
                repairService.removeDefect(this);
                return true;
            }
            return false;
        }
        return false;
    }

    public void addRepairServiceList(List<RepairService> repairServiceList) {
        this.repairServiceList.addAll(repairServiceList);
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public LocalDate getDateCreated() {
        return dateCreated;
    }

    public LocalDate getDateModified() {
        return dateModified;
    }

    public String getReporterEmail() {
        return reporterEmail;
    }

    public DefectStatus getStatus() {
        return status;
    }

    public LocatedUsing getLocatedUsing() {
        return locatedUsing;
    }

    public String getManualLocation() {
        return manualLocation;
    }

    public LocationWGS getLocationWGS() {
        return locationWGS;
    }

    public Address getAddress() {
        return address;
    }

    public List<Picture> getPictureList() {
        return pictureList;
    }

    public List<Note> getNoteList() {
        return noteList;
    }

    public List<RepairService> getRepairServiceList() {
        return repairServiceList;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setDateCreated(LocalDate date) {
        this.dateCreated = date;
    }

    public void setDateModified(LocalDate lastModifDate) {
        this.dateModified = lastModifDate;
    }

    public void setReporterEmail(String reporterEmail) {
        this.reporterEmail = reporterEmail;
    }

    public void setStatus(DefectStatus status) {
        this.status = status;
    }

    public void setLocatedUsing(LocatedUsing locatedUsing) {
        this.locatedUsing = locatedUsing;
    }

    public void setManualLocation(String manualLocation) {
        this.manualLocation = manualLocation;
    }

    public void setLocationWGS(LocationWGS locationWGS) {
        this.locationWGS = locationWGS;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public void setPictureList(List<Picture> pictureList) {
        this.pictureList = pictureList;
    }

    public void setNoteList(List<Note> noteList) {
        this.noteList = noteList;
    }

    public void setRepairServiceList(List<RepairService> repairServiceList) {
        this.repairServiceList = repairServiceList;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

}
