package cz.cvut.nahlasto.model.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum DefectStatus {
    APPROVED("Schváleno", 0),
    WAITING("Neschváleno", 1),
    DECLINED("Zamítnuto", 2),
    COMPLETED("Hotovo", 3),
    IN_PROGRESS("Pracujeme na tom", 4),
    DUPLICATE("Duplikát", 5);

    private String status;
    private int id;

    DefectStatus(String status, int id) {
        this.status = status;
        this.id = id;
    }

    private static Map<String, DefectStatus> FORMAT_MAP = Stream
            .of(DefectStatus.values())
            .collect(Collectors.toMap(s -> s.status, Function.identity()));

    @JsonCreator
    public static DefectStatus createDefectStatus(String status) {
        return Optional.ofNullable(FORMAT_MAP.get(status)).orElseThrow(() ->new IllegalArgumentException(status));
    }

    public String getStatus() {
        return status;
    }

    public int getId() {
        return id;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setId(int id) {
        this.id = id;
    }
}
