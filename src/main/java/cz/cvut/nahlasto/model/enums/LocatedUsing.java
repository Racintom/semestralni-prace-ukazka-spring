package cz.cvut.nahlasto.model.enums;

public enum LocatedUsing {
    TEXT, GPS
}
