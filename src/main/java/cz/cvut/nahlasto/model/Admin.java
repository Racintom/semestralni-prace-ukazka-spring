package cz.cvut.nahlasto.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@MappedSuperclass
public class Admin extends AbstractEntity {

    @NotNull
    @Basic
    @Column(nullable = false)
    String firstName;

    @NotNull
    @Basic
    @Column(nullable = false)
    String lastName;

    @NotNull
    @Basic
    @Column(nullable = false)
    String userName;

    @NotNull
    @Basic
    @Column(nullable = false)
    String password;

    @NotNull
    @Basic
    @Column(nullable = false)
    String email;

    Admin() {}

    public Admin(String firstName, String lastName, String userName, String password, String email) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.userName = userName;
        this.password = password;
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getUserName() {
        return userName;
    }

    public String getPassword() {
        return password;
    }

    public String getEmail() {
        return email;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "Admin{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", userName='" + userName + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
