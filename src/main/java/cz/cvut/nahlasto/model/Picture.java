package cz.cvut.nahlasto.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import cz.cvut.nahlasto.rest.util.PictureDeserializer;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "Picture")
@JsonDeserialize(using = PictureDeserializer.class)
public class Picture extends AbstractEntity {

    @NotNull
    @Column(nullable = false)
    private String path;

    @NotNull
    @Column(nullable = false)
    private boolean fromReporter;

    @JoinColumn(name = "defect")
    @ManyToOne(cascade = CascadeType.PERSIST)
    @JsonIgnoreProperties({ "pictureList" })
    private Defect defect;

    @ManyToOne(cascade = CascadeType.PERSIST)
    private CityAdmin cityAdmin;

    @Transient
    private String data;

    public Picture() {
    }

    public Picture(String path, boolean fromReporter, Defect defect, CityAdmin cityAdmin, String data) {
        this.path = path;
        this.fromReporter = fromReporter;
        this.defect = defect;
        this.cityAdmin = cityAdmin;
        this.data = data;
    }

    public void addDefect(Defect defect){
        defect.addPicture(this);
    }

    public void setDefect(Defect defect) {
        this.defect = defect;
    }

    public void addAdmin(CityAdmin admin){
        admin.addPicture(this);
    }

    public void setCityAdmin(CityAdmin cityAdmin) {
        this.cityAdmin = cityAdmin;
    }

    public String getPath() {
        return path;
    }

    public boolean isFromReporter() {
        return fromReporter;
    }

    public String getData() {
        return data;
    }

    public Defect getDefect() {
        return defect;
    }

    public CityAdmin getCityAdmin() {
        return cityAdmin;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public void setFromReporter(boolean fromReporter) {
        this.fromReporter = fromReporter;
    }

    public void setData(String data) {
        this.data = data;
    }
}
