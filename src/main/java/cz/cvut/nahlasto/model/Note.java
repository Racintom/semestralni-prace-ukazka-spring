package cz.cvut.nahlasto.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "Note")
public class Note extends AbstractEntity {

    @NotNull
    @Column(nullable = false)
    private boolean inter;

    @NotNull
    @Column(nullable = false)
    private String text;

    @JoinColumn(name = "defect")
    @ManyToOne(cascade = CascadeType.PERSIST)
    @JsonIgnoreProperties({ "noteList" })
    private Defect defect;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JsonIgnoreProperties({ "noteList", "pictureList" })
    private CityAdmin cityAdmin;

    public Note() {
    }

    public Note(boolean inter, String text, Defect defect, CityAdmin cityAdmin) {
        this.inter = inter;
        this.text = text;
        this.defect = defect;
        this.cityAdmin = cityAdmin;
    }

    public void addDefect(Defect defect){
        defect.addNote(this);
    }

    public void setDefect(Defect defect) {
        this.defect = defect;
    }

    public void addAdmin(CityAdmin admin){
        admin.addNote(this);
    }


    public void setCityAdmin(CityAdmin cityAdmin) {
        this.cityAdmin = cityAdmin;
    }

    public boolean isInter() {
        return inter;
    }

    public String getText() {
        return text;
    }

    public Defect getDefect() {
        return defect;
    }

    public CityAdmin getCityAdmin() {
        return cityAdmin;
    }

    public void setInter(boolean inter) {
        this.inter = inter;
    }

    public void setText(String text) {
        this.text = text;
    }
}