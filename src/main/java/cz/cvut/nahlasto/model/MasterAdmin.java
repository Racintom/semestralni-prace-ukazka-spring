package cz.cvut.nahlasto.model;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "MasterAdmin")
public class MasterAdmin extends Admin {

    public MasterAdmin() {}

}
