package cz.cvut.nahlasto.model;

import javax.persistence.Embeddable;

@Embeddable
public class LocationWGS {

    private Double lat;

    private Double lon;

    public LocationWGS() {
    }

    public LocationWGS(Double lat, Double lon) {
        this.lat = lat;
        this.lon = lon;
    }

    public Double getLat() {
        return lat;
    }

    public Double getLon() {
        return lon;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }
}
