package cz.cvut.nahlasto.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@SuppressWarnings("JpaQlInspection")
@Entity
@NamedQueries({
        @NamedQuery(name = "City.findAllCitiesByName",
                query = "SELECT c FROM City c WHERE c.name = :name")})
@Table(name = "City")
public class City extends AbstractEntity {

    @NotNull
    @Column(name = "city_name", nullable = false)
    private String name;

    @NotNull
    @Column(nullable = false, unique = true)
    private String postCode;

    @NotNull
    @Basic
    @Column(nullable = false)
    private String region;

    @Basic
    private String webPage;

    @Basic
    private String description;

    @Basic
    private String token;

    @OneToMany(mappedBy = "city", cascade = CascadeType.ALL)
    @JsonIgnoreProperties({"city"})
    private List<Defect> defectList;

    @OneToMany
    //@JoinTable(name = "City_Cityadmin", joinColumns = @JoinColumn(name = "city_id"), inverseJoinColumns = @JoinColumn(name = "admin_id"))
    @JsonIgnoreProperties({"noteList", "pictureList", "cityList"})
    private List<CityAdmin> adminList;

    @ManyToMany
    //@JoinTable(name = "City_RepairService", joinColumns = @JoinColumn(name = "city_id"), inverseJoinColumns = @JoinColumn(name = "repairservice_id"))
    @JsonIgnoreProperties({"cityList", "defectList"})
    private List<RepairService> repairServiceList;

    public City() {
    }

    public City(String name, String postCode, String region, String webPage, String description, List<CityAdmin> adminList, List<RepairService> repairServiceList) {
        this.name = name;
        this.postCode = postCode;
        this.region = region;
        this.webPage = webPage;
        this.description = description;
        this.adminList = adminList;
        this.repairServiceList = repairServiceList;
        generateCityToken();
    }

    public boolean addRepairService(RepairService repairService) {
        if (repairServiceList == null)
            repairServiceList = new ArrayList<>();
        if (!repairServiceList.contains(repairService)) {
            repairServiceList.add(repairService);
            repairService.addCity(this);
            return true;
        }
        return false;
    }

    public boolean removeRepairService(RepairService repairService) {
        if (repairServiceList != null) {
            if (repairServiceList.remove(repairService)) {
                repairService.removeCity(this);
                return true;
            }
            return false;
        }
        return false;
    }

    /**
     * Generate unique token for city.
     */
    private void generateCityToken() {
        token = name + postCode;
    }

    public void addDefect(Defect defect) {
        defectList.add(defect);
        defect.setCity(this);
    }

    public void addAdmin(CityAdmin admin) {
        this.adminList.add(admin);
    }

    public void addAdminList(List<CityAdmin> adminList) {
        this.adminList.addAll(adminList);
    }

    public void addRepairServiceList(List<RepairService> repairServiceList) {
        this.repairServiceList.addAll(repairServiceList);
    }

    public String getName() {
        return name;
    }

    public String getPostCode() {
        return postCode;
    }

    public String getRegion() {
        return region;
    }

    public String getWebPage() {
        return webPage;
    }

    public String getDescription() {
        return description;
    }

    public List<CityAdmin> getAdminList() {
        return adminList;
    }

    public List<RepairService> getRepairServiceList() {
        return repairServiceList;
    }

    public void setCityName(String name) {
        this.name = name;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public void setWebPage(String webPage) {
        this.webPage = webPage;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public List<Defect> getDefectList() {
        return defectList;
    }

    public void setDefectList(List<Defect> defectList) {
        this.defectList = defectList;
    }

    public void setAdminList(List<CityAdmin> adminList) {
        this.adminList = adminList;
    }

    public void setRepairServiceList(List<RepairService> repairServiceList) {
        this.repairServiceList = repairServiceList;
    }

    @Override
    public String toString() {
        return "City{" +
                "name='" + name + '\'' +
                ", postCode='" + postCode + '\'' +
                ", region='" + region + '\'' +
                ", webPage='" + webPage + '\'' +
                ", description='" + description + '\'' +
                ", token='" + token + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        City city = (City) o;
        return Objects.equals(name, city.name) &&
                Objects.equals(postCode, city.postCode) &&
                Objects.equals(region, city.region) &&
                Objects.equals(webPage, city.webPage) &&
                Objects.equals(description, city.description) &&
                Objects.equals(token, city.token);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, postCode, region, webPage, description, token);
    }
}
