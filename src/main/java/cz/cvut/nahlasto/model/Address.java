package cz.cvut.nahlasto.model;

import javax.persistence.Embeddable;

@Embeddable
public class Address {

    private String houseNumber;

    private String road;

    private String suburb;

    private String city;

    private String postcode;

    public Address() {
    }

    public Address(String houseNumber, String road, String suburb, String city, String postcode) {
        this.houseNumber = houseNumber;
        this.road = road;
        this.suburb = suburb;
        this.city = city;
        this.postcode = postcode;
    }

    public String getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
    }

    public String getRoad() {
        return road;
    }

    public void setRoad(String road) {
        this.road = road;
    }

    public String getSuburb() {
        return suburb;
    }

    public void setSuburb(String suburb) {
        this.suburb = suburb;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }
}
