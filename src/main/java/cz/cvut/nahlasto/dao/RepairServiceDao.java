/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.nahlasto.dao;

import cz.cvut.nahlasto.model.RepairService;
import org.springframework.stereotype.Repository;

@Repository
public class RepairServiceDao extends AbstractDao<RepairService> {
    
    public RepairServiceDao() {
        super(RepairService.class);
    }   
    
}
