/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.nahlasto.dao;

import cz.cvut.nahlasto.model.MasterAdmin;
import org.springframework.stereotype.Repository;

@Repository
public class MasterAdminDao extends AbstractDao<MasterAdmin> {
    
    public MasterAdminDao() {
        super(MasterAdmin.class);
    }   
    
}
