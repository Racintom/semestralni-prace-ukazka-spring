/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.nahlasto.dao;

import cz.cvut.nahlasto.model.CityAdmin;

import java.util.Objects;
import javax.persistence.NoResultException;

import org.springframework.stereotype.Repository;

@Repository
public class CityAdminDao extends AbstractDao<CityAdmin> {

    public CityAdminDao() {
        super(CityAdmin.class);
    }

    public CityAdmin findByUsername(String username) {
        Objects.requireNonNull(username);
        try {
            //noinspection JpaQueryApiInspection
            return em.createNamedQuery("CityAdmin.findByUsername", CityAdmin.class).setParameter("userName", username).getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }
}
