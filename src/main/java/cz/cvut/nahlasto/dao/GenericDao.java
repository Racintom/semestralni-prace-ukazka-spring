/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.nahlasto.dao;

import java.util.Collection;
import java.util.List;

public interface GenericDao<T> {

    T find(Integer id);

    List<T> findAll();

    List<T> findWithOffsetAndLimit(int offset, int limit);

    void create(T entity);

    void create(Collection<T> entities);

    T update(T entity);

    void remove(T entity);

    boolean exists(Integer id);
}