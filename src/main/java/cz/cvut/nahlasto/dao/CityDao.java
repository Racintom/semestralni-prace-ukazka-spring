/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.nahlasto.dao;

import cz.cvut.nahlasto.model.City;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

@Repository
public class CityDao extends AbstractDao<City> {

    private final Logger LOGGER = Logger.getLogger(CityDao.class.getSimpleName());

    public CityDao() {
        super(City.class);
    }

    public List<City> findAllCitiesByName(String name) {
        @SuppressWarnings("JpaQueryApiInspection")
        TypedQuery<City> query = em.createNamedQuery("City.findAllCitiesByName", City.class);

        query.setParameter("name", name);

        List<City> cities;

        try {
            cities = query.getResultList();
        } catch (NoResultException e) {
            return null;
        }
        return cities;
    }

    public City findCityByToken(String token) {
        @SuppressWarnings("JpaQueryApiInspection")
        TypedQuery<City> query = em.createNamedQuery("City.findCityByToken", City.class);

        query.setParameter("token", token);

        List<City> cities;

        try {
            cities = query.getResultList();
        } catch (NoResultException e) {
            return null;
        }
        if (cities.size() > 1) {
            LOGGER.log(Level.WARNING, "More cities has the same token.");
        }

        return cities.get(0);
    }

}
