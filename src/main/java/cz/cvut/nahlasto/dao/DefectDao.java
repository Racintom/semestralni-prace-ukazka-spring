/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.nahlasto.dao;

import java.time.LocalDate;
import java.util.List;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;

import cz.cvut.nahlasto.model.Defect;
import cz.cvut.nahlasto.model.enums.DefectStatus;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import java.util.List;

@Repository
public class DefectDao extends AbstractDao<Defect> {

    public DefectDao() {
        super(Defect.class);
    }

    public Defect findDefectByTitle(String title) {
        @SuppressWarnings("JpaQueryApiInspection")
        TypedQuery query = em.createNamedQuery("Defect.findAllByTitle", Defect.class);

        query.setParameter("title", title);

        Defect defect;

        try {
            defect = (Defect) query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
        return defect;
    }

    public List<Defect> findWithOffsetAndLimitAndStatus(int offset, int limit, DefectStatus status) {
        try {
            return em.createQuery("FROM " + type.getSimpleName() + " e WHERE :status = e.status", type)
                    .setParameter("status", status)
                    .setFirstResult(offset)
                    .setMaxResults(limit)
                    .getResultList();
        } finally {
            em.close();
        }
    }

    public List<Defect> findAllDefectsByStatus(DefectStatus status) {
        @SuppressWarnings("JpaQueryApiInspection")
        TypedQuery<Defect> query = em.createNamedQuery("Defect.findAllDefectsByStatus", Defect.class);

        query.setParameter("status", status);

        List<Defect> defects;

        try {
            defects = query.getResultList();
        } catch (NoResultException e) {
            return null;
        }
        return defects;
    }

    public List<Defect> findAllDefectsForUser() {
        @SuppressWarnings("JpaQueryApiInspection")
        TypedQuery<Defect> query = em.createNamedQuery("Defect.findAllDefectsForUser", Defect.class);

        List<Defect> defects;

        try {
            defects = query.getResultList();
        } catch (NoResultException e) {
            return null;
        }
        return defects;
    }

    public int coutAllDefectsForCity(Integer cityId) {
        @SuppressWarnings("JpaQueryApiInspection")
        TypedQuery<Integer> query = em.createNamedQuery("Defect.countAllDefect", Integer.class);

        return query.getSingleResult();
    }

    public int countAllRepairedForCity(Integer id) {
        @SuppressWarnings("JpaQueryApiInspection")
        TypedQuery<Integer> query = em.createNamedQuery("Defect.countAllRepairedDefect", Integer.class);

        return query.getSingleResult();
    }

    public int countAllReportedFromDate(Integer id, LocalDate date) {
        @SuppressWarnings("JpaQueryApiInspection")
        TypedQuery<Integer> query = em.createNamedQuery("Defect.countAllReportedDefect", Integer.class);

        query.setParameter("date", date);

        return query.getSingleResult();
    }

    public int countAllRepairedFromDate(Integer id, LocalDate date) {
        @SuppressWarnings("JpaQueryApiInspection")
        TypedQuery<Integer> query = em.createNamedQuery("Defect.countAllRepairedDefect", Integer.class);

        query.setParameter("date", date);

        return query.getSingleResult();
    }

    public List<Defect> findCityDefectsByToken(String cityToken) {
        @SuppressWarnings("JpaQueryApiInspection")
        TypedQuery<Defect> query = em.createNamedQuery("Defect.findCityDefectsByCityToken", Defect.class);
        List<Defect> defects;
        query.setParameter("cityToken", cityToken);

        try {
            defects = query.getResultList();
        } catch (NoResultException e) {
            return null;
        }
        return defects;
    }

    public List<Defect> findCityDefectsByCityId(Integer cityId, int offset, int limit) {
        @SuppressWarnings("JpaQueryApiInspection")
        TypedQuery<Defect> query = em.createNamedQuery("Defect.findCityDefectsByCityId", Defect.class);
        Defect defect;

        return query
                .setFirstResult(offset)
                .setMaxResults(limit)
                .getResultList();
    }

    /*public List<Defect> findAllDefectsInRange(int fr, int to) {


        @SuppressWarnings("JpaQueryApiInspection")

        TypedQuery query = em.createNamedQuery("Defect.findAllDefectsInRange", Defect.class);

        query.setParameter("fr", fr);
        query.setParameter("to", to);
        
        List<Defect> defects;

        try {
            defects = query.getResultList();
        } catch (NoResultException e) {
            return null;
        }
        return defects;
    }*/
}
