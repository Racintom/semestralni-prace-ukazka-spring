/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.nahlasto.dao;

import cz.cvut.nahlasto.model.Note;
import org.springframework.stereotype.Repository;

@Repository
public class NoteDao extends AbstractDao<Note> {
    
    public NoteDao() {
        super(Note.class);
    }   
    
}
