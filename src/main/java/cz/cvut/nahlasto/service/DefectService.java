package cz.cvut.nahlasto.service;

import cz.cvut.nahlasto.dao.DefectDao;
import cz.cvut.nahlasto.exception.ResourceAlreadyExistsException;
import cz.cvut.nahlasto.exception.ResourceNotFoundException;
import cz.cvut.nahlasto.model.CityAdmin;
import cz.cvut.nahlasto.model.Defect;
import cz.cvut.nahlasto.model.enums.DefectStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PostFilter;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PreFilter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Access;
import javax.persistence.criteria.CriteriaBuilder;
import java.util.List;
import java.util.Objects;

@Service
public class DefectService extends AbstractService<Defect> {

    @Autowired
    public DefectService(DefectDao defectDao) {
        super(defectDao, Defect.class);
    }

    @Transactional(readOnly = true)
    @PostFilter("hasAuthority('ROLE_'+ filterObject.city.id) or isAnonymous()")
    public List<Defect> findAll() {
        return abstractDao.findAll();
    }

    @Transactional(readOnly = true)
    public List<Defect> findAllDefectsForUser() {
        List<Defect> defects = ((DefectDao) abstractDao).findAllDefectsForUser();
        if (defects == null)
            throw new ResourceNotFoundException(type.getSimpleName());
        return defects;
    }

    @Transactional
    @PreAuthorize("hasAuthority('ROLE_' + #resource.city.id)")
    public void updateWithAuthorization(Defect resource) {
        abstractDao.update(resource);
    }

    @Transactional(readOnly = true)
    @PostFilter("hasAuthority('ROLE_'+ filterObject.city.id) or isAnonymous()")
    public List<Defect> findWithOffsetAndLimitAndStatus(int offset, int limit, DefectStatus status) {
        return ((DefectDao) abstractDao).findWithOffsetAndLimitAndStatus(offset, limit, status);
    }

    @Transactional(readOnly = true)
    @PostFilter("hasAuthority('ROLE_'+ filterObject.city.id) or isAnonymous()")
    public List<Defect> findAllDefectsByStatus(DefectStatus status) {
        List<Defect> defects = ((DefectDao) abstractDao).findAllDefectsByStatus(status);
        if (defects == null)
            throw new ResourceNotFoundException(type.getSimpleName(), status);
        return defects;
    }

    @Transactional(readOnly = true)
    public List<Defect> findCityDefectsByToken(String cityToken) {
        return ((DefectDao) abstractDao).findCityDefectsByToken(cityToken);
    }

    @Transactional(readOnly = true)
    public List<Defect> findCityDefectsByCityId(Integer cityId, int offset, int limit) {
        return ((DefectDao) abstractDao).findCityDefectsByCityId(cityId, offset, limit);
    }

}
