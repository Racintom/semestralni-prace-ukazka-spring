package cz.cvut.nahlasto.service;

import cz.cvut.nahlasto.dao.MasterAdminDao;
import cz.cvut.nahlasto.model.MasterAdmin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MasterAdminService extends AbstractService<MasterAdmin> {

    @Autowired
    public MasterAdminService(MasterAdminDao masterAdminDao) {
        super(masterAdminDao, MasterAdmin.class);
    }
}
