package cz.cvut.nahlasto.service;

import cz.cvut.nahlasto.dao.RepairServiceDao;
import cz.cvut.nahlasto.model.RepairService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RepairServiceService extends AbstractService<RepairService> {

    @Autowired
    protected RepairServiceService(RepairServiceDao repairServiceDao) {
        super(repairServiceDao, RepairService.class);
    }

}
