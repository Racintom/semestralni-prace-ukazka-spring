package cz.cvut.nahlasto.service;

import cz.cvut.nahlasto.dao.NoteDao;
import cz.cvut.nahlasto.model.Note;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class NoteService extends AbstractService<Note> {

    @Autowired
    public NoteService(NoteDao noteDao) {
        super(noteDao, Note.class);
    }

}
