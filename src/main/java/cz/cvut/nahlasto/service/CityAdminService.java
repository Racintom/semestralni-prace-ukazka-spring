package cz.cvut.nahlasto.service;

import cz.cvut.nahlasto.dao.CityAdminDao;
import cz.cvut.nahlasto.model.CityAdmin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CityAdminService extends AbstractService<CityAdmin> {
    @Autowired
    public CityAdminService(CityAdminDao cityAdminDao) {
        super(cityAdminDao, CityAdmin.class);
    }
}
