package cz.cvut.nahlasto.service;

import cz.cvut.nahlasto.dao.CityDao;
import cz.cvut.nahlasto.dao.DefectDao;
import cz.cvut.nahlasto.exception.ResourceNotFoundException;
import cz.cvut.nahlasto.model.City;
import cz.cvut.nahlasto.model.RepairService;
import cz.cvut.nahlasto.service.model.Statistics;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Service
public class CityService extends AbstractService<City> {

    private DefectDao defectDao;

    @Autowired
    public CityService(CityDao cityDao, DefectDao defectDao) {
        super(cityDao, City.class);
        this.defectDao = defectDao;
    }

    @Transactional(readOnly = true)
    public List<City> findAllCitiesByName(String name) {
        List<City> cities = ((CityDao) abstractDao).findAllCitiesByName(name);
        if (cities == null)
            throw new ResourceNotFoundException(type.getSimpleName(), name);
        return cities;
    }

    @Transactional(readOnly = true)
    public Statistics getStatistic(String token) {
        City city = ((CityDao) abstractDao).findCityByToken(token);
        LocalDate date = LocalDate.now();
        if (city == null)
            throw new ResourceNotFoundException(type.getSimpleName(), token);

        Statistics statistic = new Statistics();

        statistic.setTotalNumberOfDefect(defectDao.coutAllDefectsForCity(city.getId()));
        statistic.setTotalRepairedDefect(defectDao.countAllRepairedForCity(city.getId()));
        statistic.setTotalPercentage(statistic.getTotalRepairedDefect() / statistic.getTotalNumberOfDefect() * 100);
        statistic.setLastMonthReported(defectDao.countAllReportedFromDate(city.getId(), date.minusMonths(1)));
        statistic.setLastMonthRepaired(defectDao.countAllRepairedFromDate(city.getId(), date.minusMonths(1)));
        statistic.setLastYearReported(defectDao.countAllReportedFromDate(city.getId(), date.minusYears(1)));
        statistic.setLastYearRepaired(defectDao.countAllRepairedFromDate(city.getId(), date.minusYears(1)));

        return statistic;
    }

    @Transactional(readOnly = true)
    public City findCityByToken(String token) {
        City city = ((CityDao) abstractDao).findCityByToken(token);
        if (city == null)
            throw new ResourceNotFoundException(type.getSimpleName(), token);
        return city;
    }

    @Transactional()
    @PostAuthorize("!anonymous")
    public boolean addRepairService(City city, RepairService repairService) {
        if (!city.addRepairService(repairService))
            return false;
        abstractDao.update(city);
        return true;
    }

    @Transactional()
    @PostAuthorize("!anonymous")
    public boolean removeRepairService(City city, RepairService repairService) {
        if (!city.removeRepairService(repairService))
            return false;

        abstractDao.update(city);
        return true;
    }
}
