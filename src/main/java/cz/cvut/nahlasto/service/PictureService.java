package cz.cvut.nahlasto.service;

import cz.cvut.nahlasto.dao.PictureDao;
import cz.cvut.nahlasto.model.Picture;
import cz.cvut.nahlasto.rest.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.Base64;

@Service
public class PictureService extends AbstractService<Picture> {

    @Autowired
    public PictureService(PictureDao pictureDao) {
        super(pictureDao, Picture.class);
    }

    public boolean savePicture(String pictureData, Integer defectId, String pathToPicture) {
        byte[] data;
        try {
            data = Base64.getDecoder().decode(pictureData.getBytes());
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            return false;
        }
        return createAndWriteToFile(data, defectId, pathToPicture);
    }

    public boolean savePicture(byte[] bytes, Integer defectId, String pathToPicture) {
        return createAndWriteToFile(bytes, defectId, pathToPicture);
    }

    private boolean createAndWriteToFile(byte[] bytes, Integer defectId, String pathToFile) {
        File imgDirectory = new File(Constants.IMG_DIR); // Složka pro fotky
        File defectDirectory = new File(Constants.PATH_TO_DEF_IMG_DIR_PREFIX + defectId); // Složka s fotkami pro daný defekt
        if (!imgDirectory.exists()) imgDirectory.mkdir();
        if (!defectDirectory.exists()) defectDirectory.mkdir();
        try (OutputStream stream = new FileOutputStream(pathToFile)) {
            stream.write(bytes);
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public void deletePicture(String pathToPicture) {
        new File(pathToPicture).delete();
    }

}
