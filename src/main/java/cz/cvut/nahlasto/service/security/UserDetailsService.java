package cz.cvut.nahlasto.service.security;

import cz.cvut.nahlasto.dao.CityAdminDao;
import cz.cvut.nahlasto.model.CityAdmin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserDetailsService implements org.springframework.security.core.userdetails.UserDetailsService {

    private final CityAdminDao userDao;

    @Autowired
    public UserDetailsService(CityAdminDao userDao) {
        this.userDao = userDao;
    }

    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        final CityAdmin user = userDao.findByUsername(username);

        if (user == null) {
            throw new UsernameNotFoundException("User with username " + username + " not found.");
        }
        return new cz.cvut.nahlasto.security.model.UserDetails(user);
    }
}