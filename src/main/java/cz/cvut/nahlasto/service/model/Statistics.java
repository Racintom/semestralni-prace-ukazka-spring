package cz.cvut.nahlasto.service.model;

public class Statistics {

    private int totalNumberOfDefect;
    private int totalRepairedDefect;
    private double totalPercentage;

    private int lastYearReported;
    private int lastYearRepaired;

    private int lastMonthReported;
    private int lastMonthRepaired;

    public Statistics() {
    }

    public Statistics(int totalNumberOfDefect, int totalRepairedDefect, double totalPercentage, int lastYearReported, int lastYearRepaired, int lastMonthReported, int lastMonthRepaired) {
        this.totalNumberOfDefect = totalNumberOfDefect;
        this.totalRepairedDefect = totalRepairedDefect;
        this.totalPercentage = totalPercentage;
        this.lastYearReported = lastYearReported;
        this.lastYearRepaired = lastYearRepaired;
        this.lastMonthReported = lastMonthReported;
        this.lastMonthRepaired = lastMonthRepaired;
    }

    public int getTotalNumberOfDefect() {
        return totalNumberOfDefect;
    }

    public void setTotalNumberOfDefect(int totalNumberOfDefect) {
        this.totalNumberOfDefect = totalNumberOfDefect;
    }

    public int getTotalRepairedDefect() {
        return totalRepairedDefect;
    }

    public void setTotalRepairedDefect(int totalRepairedDefect) {
        this.totalRepairedDefect = totalRepairedDefect;
    }

    public double getTotalPercentage() {
        return totalPercentage;
    }

    public void setTotalPercentage(double totalPercentage) {
        this.totalPercentage = totalPercentage;
    }

    public int getLastYearReported() {
        return lastYearReported;
    }

    public void setLastYearReported(int lastYearReported) {
        this.lastYearReported = lastYearReported;
    }

    public int getLastYearRepaired() {
        return lastYearRepaired;
    }

    public void setLastYearRepaired(int lastYearRepaired) {
        this.lastYearRepaired = lastYearRepaired;
    }

    public int getLastMonthReported() {
        return lastMonthReported;
    }

    public void setLastMonthReported(int lastMonthReported) {
        this.lastMonthReported = lastMonthReported;
    }

    public int getLastMonthRepaired() {
        return lastMonthRepaired;
    }

    public void setLastMonthRepaired(int lastMonthRepaired) {
        this.lastMonthRepaired = lastMonthRepaired;
    }
}
