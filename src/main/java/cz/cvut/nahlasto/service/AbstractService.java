package cz.cvut.nahlasto.service;

import cz.cvut.nahlasto.dao.AbstractDao;
import cz.cvut.nahlasto.exception.ResourceAlreadyExistsException;
import cz.cvut.nahlasto.exception.ResourceNotFoundException;
import cz.cvut.nahlasto.model.AbstractEntity;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PreFilter;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

public abstract class AbstractService<T extends AbstractEntity> {

    protected final Class<T> type;
    protected AbstractDao<T> abstractDao;

    protected AbstractService(AbstractDao<T> abstractDao, Class<T> type) {
        this.abstractDao = abstractDao;
        this.type = type;
    }

    @Transactional(readOnly = true)
    public T findById(Integer id) {
        T resource = abstractDao.find(id);
        if (resource == null)
            throw new ResourceNotFoundException(type.getSimpleName(), id);
        return resource;
    }

    @Transactional(readOnly = true)
    public List<T> findAll() {
        return abstractDao.findAll();
    }

    @Transactional(readOnly = true)
    public List<T> findWithOffsetAndLimit(int offset, int limit) {
        return abstractDao.findWithOffsetAndLimit(offset, limit);
    }

    @Transactional
    public void create(T resource) {
        Objects.requireNonNull(resource);
        if (!exists(resource.getId()))
            abstractDao.create(resource);
        else
            throw new ResourceAlreadyExistsException(type.getSimpleName(), resource.getId());
    }

    @Transactional
    @PreAuthorize("!anonymous")
    public void createWithAuthorization(T resource) {
        Objects.requireNonNull(resource);
        if (!exists(resource.getId()))
            abstractDao.create(resource);
        else
            throw new ResourceAlreadyExistsException(type.getSimpleName(), resource.getId());
    }

    @Transactional
    @PreAuthorize("!anonymous")
    public void createList(Collection<T> resources) {
        abstractDao.create(resources);
    }

    @Transactional
    public void update(T resource) {
        abstractDao.update(resource);
    }

    @Transactional
    @PreAuthorize("!anonymous")
    public void updateWithAuthorization(T resource) {
        abstractDao.update(resource);
    }

    @Transactional
    @PreAuthorize("!isAnonymous()")
    public void remove(T resource) {
        abstractDao.remove(resource);
    }

    @Transactional(readOnly = true)
    public boolean exists(Integer id) {
        return abstractDao.exists(id);
    }

}
