package cz.cvut.nahlasto.exception;

import cz.cvut.nahlasto.model.enums.DefectStatus;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class ResourceNotFoundException extends NahlasToException {

    public ResourceNotFoundException(String resourceName) {
        super("Resource by name '" + resourceName + "' was not found.");
    }

    public ResourceNotFoundException(String resourceName, Integer identifier) {
        super("Resource by name '" + resourceName +
              "' with identifier '" + identifier.toString() + "' was not found.");
    }

    public ResourceNotFoundException(String resourceName, String name) {
        super("Resource by name '" + resourceName +
                "' with String attribute with value '" + name + "' was not found.");
    }

    public ResourceNotFoundException(String resourceName, DefectStatus status) {
        super("Resource by name '" + resourceName +
                "' with DefectStatus attribute with value '" + status.toString() + "' was not found.");
    }
}
