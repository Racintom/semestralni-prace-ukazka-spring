package cz.cvut.nahlasto.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class ResourceAlreadyExistsException extends NahlasToException {

    public ResourceAlreadyExistsException(String message) {
        super(message);
    }

    public ResourceAlreadyExistsException(String resourceName, Integer identifier) {
        super("Resource by name '" + resourceName +
                "' with identifier '" + identifier.toString() + "' already exists.");
    }
}
