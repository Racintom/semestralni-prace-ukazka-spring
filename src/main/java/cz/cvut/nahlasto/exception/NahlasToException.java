package cz.cvut.nahlasto.exception;

public class NahlasToException extends RuntimeException {

    public NahlasToException(String message) {
        super(message);
    }
}
