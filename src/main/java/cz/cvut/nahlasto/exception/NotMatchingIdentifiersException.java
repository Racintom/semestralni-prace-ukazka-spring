package cz.cvut.nahlasto.exception;

public class NotMatchingIdentifiersException extends NahlasToException {

    public NotMatchingIdentifiersException(String resourceName, Integer originalIdentifier, Integer requestBodyIdentifier) {
        super(resourceName + "'s original identifier ('" + originalIdentifier + "') " +
                "does not match with the one in the request body ('" + requestBodyIdentifier + "')");
    }

}
